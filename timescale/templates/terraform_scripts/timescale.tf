variable "timescale_instance_ami" {
  type        = string
  description = "Timescale vm instance ami"
}

variable "timescale_instance_type" {
  type        = string
  description = "Timescale vm instance type"
}

variable "timescale_keypair_name" {
  type        = string
  description = "Timescale vm instance keypair name"
}

variable "timescale_associate_public_ip_address" {
  type        = string
  description = "Define is Timescale vm instance have public ip [true/false]"
}

variable "timescale_subnet_id" {
  type        = string
  description = "Timescale vm subnet id"
}

variable "timescale_volume_size" {
  type        = string
  description = "Timescale vm instance volume size"
}

variable "var.env_nameprefix" {
  type        = string
  description = "Environment prefix used for naming resources"
}

variable "aws_vpc" {
  type        = string
  description = "Timescale vm AWS vpc"
}

variable "timescale_ebs_snapshot_id" {
  type        = string
  description = "Snapshot id of AMI's EBS"
}

variable "mgmt_subnet_cidr" {
  type        = string
  description = "Management station CIDR"
}

# Configure AWS instance
resource "aws_instance" "timescale_<%=node_index%>" {
  ami                         = var.timescale_instance_ami
  instance_type               = var.timescale_instance_type
  key_name                    = var.timescale_keypair_name
  associate_public_ip_address = var.timescale_associate_public_ip_address
  subnet_id                   = var.timescale_subnet_id
  vpc_security_group_ids      = [aws_security_group.timescale_<%=node_index%>.id]
  ebs_block_device {
    device_name = "/dev/sda1"
    volume_size = var.timescale_volume_size
    snapshot_id = var.timescale_ebs_snapshot_id
  }

  tags = {
    Environment = var.env_nameprefix
    Name        = "${var.env_nameprefix}_timescale_<%=node_index%>
  }
}

# Configure AWS security group
resource "aws_security_group" "timescale_<%=node_index%>" {
  name        = "var.env_nameprefix_timescale_<%=node_index%>"
  description = "timescale access"
  vpc_id      = var.aws_vpc

  ingress {
    description = "access_from_mgmt"
    from_port   = 0
    to_port     = 65535
    protocol    = "tcp"
    cidr_blocks = [var.mgmt_subnet_cidr]
  }

  ingress {
    description = "access_to_timescale"
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Environment = var.env_nameprefix
    Name        = "${var.env_nameprefix}_timescale_<%=node_index%>_sg"
  }
}

# Save timescale instance id
output "timescale_<%=node_index%>_id" {
  value       = aws_instance.timescale_<%=node_index%>.id
  description = "The unique identifier of the timescale virtual machine."
}

# Save timescale security group id
output "timescale_<%=node_index%>_sg_id" {
  value       = aws_security_group.timescale_<%=node_index%>.id
  description = "The unique identifier of the timescale security group."
}

# Save timescale instance private ip
output "timescale_<%=node_index%>_private_ip" {
  value       = aws_instance.timescale_<%=node_index%>.private_ip
  description = "The private IP address of the timescale virtual machine."
}

# Save timescale instance public ip
output "timescale_<%=node_index%>_public_ip" {
  value       = aws_instance.timescale_<%=node_index%>.public_ip
  description = "The public IP address of the timescale virtual machine."
}
