#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [string] $ConfigPath,

    [Parameter(Mandatory = $true, Position = 1)]
    [string] $ResourcePath
)

# Read config and resources
$config = Read-EnvConfig -ConfigPath $ConfigPath
$resources = Read-EnvResources -ResourcePath $ResourcePath

################################## Timescale ##################################
# Prepare terraform files and set terraform env vars
. "$PSScriptRoot/terraform.ps1" -Config $config -Resources $resources

# Delete aws resources
Set-Location $timescaleTerraformPath

terraform init
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "timescale" "Can't initialize terraform. Watch logs above or check '$timescaleTerraformPath' folder content."
}

terraform plan
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "timescale" "Can't execute terraform plan. Watch logs above or check '$timescaleTerraformPath' folder content."
}

terraform destroy -auto-approve
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "timescale" "Can't delete cloud resources. Watch logs above or check '$timescaleTerraformPath' folder content."
}

Set-Location "$PSScriptRoot/.."

# Delete terraform state from config folder
Remove-State -Component "timescale" -EnvPrefix $(Get-EnvMapValue -Map $config -Key "environment.prefix")

for ($i = 0; $i -lt $timescaleVMCount; $i++) {
    # Remove from resources
    if (Test-EnvMapValue -Map $resources -Key "timescale") {
        if (Test-EnvMapValue -Map $resources -Key "timescale.node_$($i)") {
            Remove-EnvMapValue -Map $resources -Key "timescale.node_$($i).private_ip"
            Remove-EnvMapValue -Map $resources -Key "timescale.node_$($i).public_ip"
            Remove-EnvMapValue -Map $resources -Key "timescale.node_$($i).id"
            Remove-EnvMapValue -Map $resources -Key "timescale.node_$($i).sg_id"
        }
    }
}
        
Write-EnvResources -ResourcePath $ResourcePath -Resources $resources
###################################################################

################################## Keypair ##################################
# Prepare terraform files and set terraform env vars
. "$PSScriptRoot/terraform.ps1" -Config $config -Resources $resources

# Delete aws resources
Set-Location $keypairTerraformPath

terraform init
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "timescale" "Can't initialize terraform. Watch logs above or check '$keypairTerraformPath' folder content."
}

terraform plan
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "timescale" "Can't execute terraform plan. Watch logs above or check '$keypairTerraformPath' folder content."
}

terraform destroy -auto-approve
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "timescale" "Can't delete cloud resources. Watch logs above or check '$keypairTerraformPath' folder content."
}

Set-Location "$PSScriptRoot/.."

# Delete terraform state from config folder
Remove-State -Component "keypair_timescale" -EnvPrefix $(Get-EnvMapValue -Map $config -Key "environment.prefix")

# Delete results and save resource file to disk

if (Test-EnvMapValue -Map $resources -Key "timescale") {
    Remove-EnvMapValue -Map $resources -Key "timescale.keypair_name"
    Write-EnvResources -ResourcePath $ResourcePath -Resources $resources
}
###################################################################