#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [hashtable] $Config,

    [Parameter(Mandatory = $true, Position = 1)]
    [hashtable] $Resources
)

# Prepare hosts file
$ansibleInventory = @("[master]")
$ansibleInventory += "0 ansible_host=$(Get-EnvMapValue -Map $resources -Key "timescale.node_0.private_ip") ansible_ssh_user=$(Get-EnvMapValue -Map $config -Key "timescale.node_username") ansible_ssh_private_key_file=$(Get-EnvMapValue -Map $resources -Key "timescale.keypair_name")"
$ansibleInventory += "`n[standby]"
$timescaleVMCount = $(Get-EnvMapValue -Map $config -Key "timescale.nodes_count")
for ($i = 1; $i -lt $timescaleVMCount; $i++) {
    $ansibleInventory += "$i ansible_host=$(Get-EnvMapValue -Map $resources -Key "timescale.node_$i`.private_ip") ansible_ssh_user=$(Get-EnvMapValue -Map $config -Key "timescale.node_username") ansible_ssh_private_key_file=$(Get-EnvMapValue -Map $resources -Key "timescale.keypair_name")"
}
Set-Content -Path "$path/../temp/timescale_ansible_hosts" -Value $ansibleInventory

# Whitelist nodes
if ($config["extended_ansible_logs"]) {
    ansible-playbook -v -i "$path/../temp/timescale_ansible_hosts" "$path/templates/ssh_keyscan_playbook.yml"
} else {
    ansible-playbook -i "$path/../temp/timescale_ansible_hosts" "$path/templates/ssh_keyscan_playbook.yml"
}
