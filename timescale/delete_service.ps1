#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [string] $ConfigPath,

    [Parameter(Mandatory = $true, Position = 1)]
    [string] $ResourcePath
)

# Read config and resources
$config = Read-EnvConfig -ConfigPath $ConfigPath
$resources = Read-EnvResources -ResourcePath $ResourcePath

# Prepare hosts file if missing
if (!(Test-Path -Path "$path/../temp/timescale_ansible_hosts")) {
    # Prepare ansible hosts file and whitelist nodes
    . "$PSScriptRoot/ansible.ps1" -Config $config -Resources $resources
}

# Destroy timescale database
$version = Get-EnvMapValue -Map $config -Key "timescale.version"

$templateParams = @{ 
    version=$version; 
}

Build-EnvTemplate -InputPath "$($path)/templates/timescale_uninstall_playbook.yml" -OutputPath "$($path)/../temp/timescale_uninstall_playbook.yml" -Params1 $templateParams

if ($config["extended_ansible_logs"]) {
    ansible-playbook -v -i "$path/../temp/timescale_ansible_hosts" "$path/../temp/timescale_uninstall_playbook.yml"
} else {
    ansible-playbook -i "$path/../temp/timescale_ansible_hosts" "$path/../temp/timescale_uninstall_playbook.yml"
}

# Delete results and save resource file to disk
if (Test-EnvMapValue -Map $resources -Key "timescale") {
    Remove-EnvMapValue -Map $resources -Key ".endpoint"
    Remove-EnvMapValue -Map $resources -Key "timescale.inventory"
    Remove-EnvMapValue -Map $resources -Key "timescale.host"
    Remove-EnvMapValue -Map $resources -Key "timescale.port"
}
Write-EnvResources -ResourcePath $ResourcePath -Resources $resources
