#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [string] $ConfigPath,

    [Parameter(Mandatory = $true, Position = 1)]
    [string] $ResourcePath
)

# Read config and resources
$config = Read-EnvConfig -ConfigPath $ConfigPath
$resources = Read-EnvResources -ResourcePath $ResourcePath

# Check timescaleql version
if ((Get-EnvMapValue -Map $config -Key "timescale.timescale_version") -lt 11) {
    Write-EnvError -Component "timescale" "Use timescale version 11 or higher to install timescale db."
}

# Prepare ansible hosts file and whitelist nodes
. "$PSScriptRoot/ansible.ps1" -Config $config -Resources $resources

# Install timescale database
$version = Get-EnvMapValue -Map $config -Key "timescale.timescale_version"
$name = Get-EnvMapValue -Map $config -Key "timescale.name"
$username = Get-EnvMapValue -Map $config -Key "timescale.username"
$password = Get-EnvMapValue -Map $config -Key "timescale.password"
$hw_timescale_master_ip = Get-EnvMapValue -Map $config -Key "timescale.node_0.private_ip"
$standby_replications_list = ""
for ($i = 1; $i -lt $timescaleVMCount; $i++) {
    $standby_replications_list += "host replication repuser $(Get-EnvMapValue -Map $config -Key "timescale.node_$i`.private_ip")/32 trust"
}

$templateParams = @{ 
    version=$version;
    name=$name;
    username=$username;
    password=$password;
    standby_replications_list=$standby_replications_list;
    hw_timescale_master_ip=$hw_timescale_master_ip;
}

Build-EnvTemplate -InputPath "$($path)/templates/timescale_install_playbook.yml" -OutputPath "$($path)/../temp/timescale_install_playbook.yml" -Params1 $templateParams

if ($config["extended_ansible_logs"]) {
    ansible-playbook -v -i "$path/../temp/timescale_ansible_hosts" "$path/../temp/timescale_install_playbook.yml"
} else {
    ansible-playbook -i "$path/../temp/timescale_ansible_hosts" "$path/../temp/timescale_install_playbook.yml"
}

# Write timescale resources
$port="5432"
$ip=(Get-EnvMapValue -Map $resources -Key "timescale.node_0.private_ip")
$endpoint = "$($ip):$($port)"
Set-EnvMapValue -Map $resources -Key "timescale.host" -Value $ip
Set-EnvMapValue -Map $resources -Key "timescale.port" -Value $port
Set-EnvMapValue -Map $resources -Key "timescale.endpoint" -Value $endpoint
Set-EnvMapValue -Map $resources -Key "timescale.inventory" -Value $ansibleInventory
Write-EnvResources -ResourcePath $ResourcePath -Resources $resources