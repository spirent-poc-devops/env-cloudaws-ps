#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [string] $ConfigPath,

    [Parameter(Mandatory = $true, Position = 1)]
    [string] $ResourcePath
)

# Read config and resources
$config = Read-EnvConfig -ConfigPath $ConfigPath
$resources = Read-EnvResources -ResourcePath $ResourcePath

############################## Keypair #####################################
# Prepare terraform files and set terraform env vars
. "$PSScriptRoot/terraform.ps1" -Config $config -Resources $resources

Set-Location $keypairTerraformPath

# Initialize terraform
Write-Host "Initializing terraform..."
terraform init
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "timescale" "Can't initialize terraform. Watch logs above or check '$keypairTerraformPath' folder content."
}

# Plan cloud resources creation
Write-Host "Executing terraform plan command..."
terraform plan
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "timescale" "Can't execute terraform plan. Watch logs above or check '$keypairTerraformPath' folder content."

}

# Create cloud resources
Write-Host "Creating cloud resources by terraform..."
terraform apply -auto-approve
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
        Write-EnvError -Component "timescale" "Can't create cloud resources. Watch logs above or check '$keypairTerraformPath' folder content."
}

# Get keypair name
$keypair=$(terraform output keypair_name).Trim('"')
Set-EnvMapValue -Map $resources -Key "timescale.keypair_name" -Value $keypair

Set-Location "$PSScriptRoot/.."

# Save terraform state to config folder
Save-State -Component "keypair_timescale" -EnvPrefix $(Get-EnvMapValue -Map $config -Key "environment.prefix")

Write-EnvResources -ResourcePath $ResourcePath -Resources $resources
###################################################################

############################### Timescale Virtual Machines ####################################
# Prepare terraform files and set terraform env vars
. "$PSScriptRoot/terraform.ps1" -Config $config -Resources $resources

Set-Location $timescaleTerraformPath

# Initialize terraform
Write-Host "Initializing terraform..."
terraform init
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "timescale" "Can't initialize terraform. Watch logs above or check '$keypairTerraformPath' folder content."
}

# Plan cloud resources creation
Write-Host "Executing terraform plan command..."
terraform plan
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "timescale" "Can't execute terraform plan. Watch logs above or check '$keypairTerraformPath' folder content."
}

# Create cloud resources
Write-Host "Creating cloud resources by terraform..."
terraform apply -auto-approve
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "timescale" "Can't create cloud resources. Watch logs above or check '$keypairTerraformPath' folder content."
}

for ($i = 0; $i -lt $timescaleVMCount; $i++) {
    # Get outputs
    $timescale_id=$(terraform output "timescale_$($i)_id").Trim('"')
    $timescale_private_ip=$(terraform output "timescale_$($i)_private_ip").Trim('"')
    $timescale_public_ip=$(terraform output "timescale_$($i)_public_ip").Trim('"')
    $timescale_sg_id=$(terraform output "timescale_$($i)_sg_id").Trim('"')

    Set-EnvMapValue -Map $resources -Key "timescale.node_$($i).id" -Value $timescale_id
    Set-EnvMapValue -Map $resources -Key "timescale.node_$($i).private_ip" -Value $timescale_private_ip
    Set-EnvMapValue -Map $resources -Key "timescale.node_$($i).public_ip" -Value $timescale_public_ip
    Set-EnvMapValue -Map $resources -Key "timescale.node_$($i).sg_id" -Value $timescale_sg_id
}

Set-Location "$PSScriptRoot/.."

# Save terraform state to config folder
Save-State -Component "timescale" -EnvPrefix $(Get-EnvMapValue -Map $config -Key "environment.prefix")

Write-EnvResources -ResourcePath $ResourcePath -Resources $resources
#####################################################################################