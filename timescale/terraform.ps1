#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [hashtable] $Config,

    [Parameter(Mandatory = $true, Position = 1)]
    [hashtable] $Resources
)

############################## Keypair #####################################
$keypairTerraformPath = "$PSScriptRoot/../temp/keypair_timescale_$(Get-EnvMapValue -Map $config -Key "environment.prefix")"
if (!(Test-Path $keypairTerraformPath)) {
    $null = New-Item $keypairTerraformPath -ItemType "directory"
} 

$env:TF_VAR_aws_cred_file = Get-EnvMapValue -Map $config -Key "aws.credentials_file"
$env:TF_VAR_aws_profile = Get-EnvMapValue -Map $config -Key "aws.profile"
$env:TF_VAR_aws_region = Get-EnvMapValue -Map $config -Key "aws.region"
$env:TF_VAR_aws_stage = Get-EnvMapValue -Map $config -Key "aws.stage"
$env:TF_VAR_aws_namespace = Get-EnvMapValue -Map $config -Key "aws.namespace"
$env:TF_VAR_env_nameprefix = Get-EnvMapValue -Map $config -Key "environment.prefix"

Copy-Item -Path "$PSScriptRoot/templates/terraform_scripts/provider.tf" -Destination "$keypairTerraformPath/provider.tf"
Copy-Item -Path "$PSScriptRoot/templates/terraform_scripts/keypair.tf" -Destination "$keypairTerraformPath/keypair.tf"

# Load terraform state from config folder
Sync-State -Component "keypair_timescale" -EnvPrefix $(Get-EnvMapValue -Map $config -Key "environment.prefix")
###################################################################

################################## Timescale Virtual Machine #################################
$timescaleTerraformPath = "$PSScriptRoot/../temp/timescale_$(Get-EnvMapValue -Map $config -Key "environment.prefix")"

$env:TF_VAR_aws_cred_file = Get-EnvMapValue -Map $config -Key "aws.credentials_file"
$env:TF_VAR_aws_profile = Get-EnvMapValue -Map $config -Key "aws.profile"
$env:TF_VAR_aws_role_name = Get-EnvMapValue -Map $config -Key "aws.terraform_role_name"
$env:TF_VAR_aws_region = Get-EnvMapValue -Map $config -Key "aws.region"
$env:TF_VAR_timescale_instance_ami = Get-EnvMapValue -Map $config -Key "timescale.instance_ami"
$env:TF_VAR_timescale_instance_type = Get-EnvMapValue -Map $config -Key "timescale.instance_type"
$env:TF_VAR_timescale_keypair_name = Get-EnvMapValue -Map $resources -Key "timescale.keypair_name"
$env:TF_VAR_timescale_associate_public_ip_address = Get-EnvMapValue -Map $resources -Key "timescale.associate_public_ip_address"
$env:TF_VAR_timescale_subnet_id = Get-EnvMapValue -Map $config -Key "timescale.subnet_id"
$env:TF_VAR_env_nameprefix = Get-EnvMapValue -Map $config -Key "environment.prefix"
$env:TF_VAR_aws_vpc = Get-EnvMapValue -Map $config -Key "aws.vpc"
$env:TF_VAR_mgmt_subnet_cidr = Get-EnvMapValue -Map $config -Key "mgmtstation.subnet_cidr"
$env:TF_VAR_timescale_volume_size = Get-EnvMapValue -Map $config -Key "timescale.volume_size"
$env:TF_VAR_timescale_associate_public_ip_address = Get-EnvMapValue -Map $config -Key "timescale.associate_public_ip_address"
$env:TF_VAR_timescale_ebs_snapshot_id = Get-EnvMapValue -Map $config -Key "timescale.ebs_snapshot_id"

Copy-Item -Path "$PSScriptRoot/templates/terraform_scripts/provider.tf" -Destination "$keypairTerraformPath/provider.tf"

$timescaleVMCount = $(Get-EnvMapValue -Map $config -Key "timescale.nodes_count")
for ($i = 0; $i -lt $timescaleVMCount; $i++) {
    # Select cloud terraform templates/terraform_scripts

    $templateParams = @{ 
        node_index=$i
    }

    Build-EnvTemplate -InputPath "$PSScriptRoot/templates/terraform_scripts/timescale.tf" -OutputPath "$PSScriptRoot/../temp/timescale_tf/timescale_$i.tf" -Params1 $templateParams
}
# Load terraform state from config folder
Sync-State -Component "timescale" -EnvPrefix $(Get-EnvMapValue -Map $config -Key "environment.prefix")
###################################################################