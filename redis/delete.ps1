#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [string] $ConfigPath,

    [Parameter(Mandatory = $true, Position = 1)]
    [string] $ResourcePath
)

# Read config and resources
$config = Read-EnvConfig -ConfigPath $ConfigPath
$resources = Read-EnvResources -ResourcePath $ResourcePath

# Prepare terraform files and set terraform env vars
. "$PSScriptRoot/terraform.ps1" -Config $config -Resources $resources

# Delete aws redis resources
Set-Location $redisTerraformPath
terraform init
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "redis" "Can't initialize terraform. Watch logs above or check '$redisTerraformPath' folder content."
}

terraform plan
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "redis" "Can't execute terraform plan. Watch logs above or check '$redisTerraformPath' folder content."
}

terraform destroy -auto-approve
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "redis" "Can't delete cloud resources. Watch logs above or check '$redisTerraformPath' folder content."
}

Set-Location "$PSScriptRoot/.."

# Delete terraform state from config folder
Remove-State -Component "redis" -EnvPrefix $(Get-EnvMapValue -Map $config -Key "environment.prefix")

# Delete results and save resource file to disk
if (Test-EnvMapValue -Map $resources -Key "redis") {
    Remove-EnvMapValue -Map $resources -Key "redis.name"
    Remove-EnvMapValue -Map $resources -Key "redis.host"
    Remove-EnvMapValue -Map $resources -Key "redis.endpoint"
    Remove-EnvMapValue -Map $resources -Key "redis.port"
    Remove-EnvMapValue -Map $resources -Key "redis.security_group_id"
}

Write-EnvResources -ResourcePath $ResourcePath -Resources $resources
    