#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [string] $ConfigPath,

    [Parameter(Mandatory = $true, Position = 1)]
    [string] $ResourcePath
)

# Read config and resources
$config = Read-EnvConfig -ConfigPath $ConfigPath
$resources = Read-EnvResources -ResourcePath $ResourcePath

# Prepare terraform files and set terraform env vars
. "$PSScriptRoot/terraform.ps1" -Config $config -Resources $resources

Set-Location $redisTerraformPath
Write-Host "Initializing terraform..."
terraform init
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "redis" "Can't initialize terraform. Watch logs above or check '$redisTerraformPath' folder content."
}

# Plan cloud resources creation
Write-Host "Executing terraform plan command..."
terraform plan
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "redis" "Can't execute terraform plan. Watch logs above or check '$redisTerraformPath' folder content."
}

# Create cloud resources
Write-Host "Creating cloud resources by terraform..."
terraform apply -auto-approve
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "redis" "Can't create cloud resources. Watch logs above or check '$redisTerraformPath' folder content."
}

#Output values
$redisHost=$(terraform output endpoint).Trim('"')
$redisId=$(terraform output id).Trim('"')
$redisPort=$(terraform output port).Trim('"')
$redisEndpoint="$($redisHost):$($redisPort)"
$redisSGId=$(terraform output redis_security_group_id).Trim('"')

Set-Location -Path "$PSScriptRoot/.."

#Elastic Cache Redis Details
Set-EnvMapValue -Map $resources -Key "redis.name" -Value $redisId
Set-EnvMapValue -Map $resources -Key "redis.host" -Value $redisHost
Set-EnvMapValue -Map $resources -Key "redis.endpoint" -Value $redisEndpoint
Set-EnvMapValue -Map $resources -Key "redis.port" -Value $redisPort
Set-EnvMapValue -Map $resources -Key "redis.security_group_id" -Value $redisSGId

# Save terraform state to config folder
Save-State -Component "kafka" -EnvPrefix $(Get-EnvMapValue -Map $config -Key "environment.prefix")

Write-EnvResources -ResourcePath $ResourcePath -Resources $resources
