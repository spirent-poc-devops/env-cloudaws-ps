#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [hashtable] $Config,

    [Parameter(Mandatory = $true, Position = 2)]
    [hashtable] $Resources
)

$redisTerraformPath = "$PSScriptRoot/../temp/redis_$(Get-EnvMapValue -Map $config -Key "environment.prefix")"
if (!(Test-Path $redisTerraformPath)) {
    $null = New-Item $redisTerraformPath -ItemType "directory"
}

$env:TF_VAR_aws_region = Get-EnvMapValue -Map $config -Key "aws.region"
$env:TF_VAR_aws_credentials_file = Get-EnvMapValue -Map $config -Key "aws.credentials_file"
$env:TF_VAR_aws_profile = Get-EnvMapValue -Map $config -Key "aws.profile"
$env:TF_VAR_vpc = Get-EnvMapValue -Map $config -Key "aws.vpc"
#subnets 
$publicSubnets = (Get-EnvMapValue -Map $config -Key "aws.public_subnets") -join '","'
$publicSubnets = '["' + $publicSubnets + '"]'
$env:TF_VAR_public_subnets = $publicSubnets
$env:TF_VAR_vpc_default_sg = Get-EnvMapValue -Map $config -Key "aws.vpc_default_sg"

$env:TF_VAR_account_id = Get-EnvMapValue -Map $config -Key "aws.account_id"
$env:TF_VAR_namespace = Get-EnvMapValue -Map $config -Key "aws.namespace"
$env:TF_VAR_stage = Get-EnvMapValue -Map $config -Key "aws.stage"
$env:TF_VAR_env_nameprefix = Get-EnvMapValue -Map $config -Key "environment.prefix"

#Elastic Cache Redis Variables
$env:TF_VAR_cluster_size = Get-EnvMapValue -Map $config -Key "redis.cluster_size"
$env:TF_VAR_redis_instance_type = Get-EnvMapValue -Map $config -Key  "redis.instance_type"
$env:TF_VAR_redis_engine_version = Get-EnvMapValue -Map $config -Key "redis.engine_version"
$env:TF_VAR_redis_family = Get-EnvMapValue -Map $config -Key "redis.family"
$env:TF_VAR_redis_port = Get-EnvMapValue -Map $config -Key "redis.port"
$env:TF_VAR_redis_instance_type = Get-EnvMapValue -Map $config -Key "redis.instance_type"
$env:TF_VAR_aws_region = Get-EnvMapValue -Map $config -Key "aws.region"
$env:TF_VAR_kms_key_id = Get-EnvMapValue -Map $config -Key "aws.kms_key_id"

Copy-Item -Path "$PSScriptRoot/templates/terraform_scripts/main.tf" -Destination "$redisTerraformPath/main.tf"
Copy-Item -Path "$PSScriptRoot/templates/terraform_scripts/msk.tf" -Destination "$redisTerraformPath/msk.tf"
Copy-Item -Path "$PSScriptRoot/templates/terraform_scripts/output.tf" -Destination "$redisTerraformPath/output.tf"
Copy-Item -Path "$PSScriptRoot/templates/terraform_scripts/variables.tf" -Destination "$redisTerraformPath/variables.tf"

# Load terraform state from config folder
Sync-State -Component "redis" -EnvPrefix $(Get-EnvMapValue -Map $config -Key "environment.prefix")
