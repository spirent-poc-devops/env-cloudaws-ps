
variable "aws_region" {
  type        = string
  description = "AWS Region"
}

variable "aws_credentials_file" {
  type        = string
  description = "AWS Credentials File"
}

variable "aws_profile" {
  type        = string
  description = "AWS Profile"
}

variable "vpc" {
  type        = string
  description = "AWS VPC"
}

variable "vpc_default_sg"{
  type = string
  default = "AWS VPC default SG"
}

variable "public_subnets" {
  type        = list
  description = "AWS Public Subnets"
}

variable "enabled" {
  type        = string
  description = "Whether to create the resources. Set to `false` to prevent the module from creating any resources"
  default     = true
}

variable "account_id" {
  description = "Account ID of the AWS account"
}

variable "namespace" {
  description = "Namespace (e.g. `cp` or `cloudposse`)"
}

variable "env_nameprefix" {
  type        = string
  description = "Name Prefix"
}

variable "stage" {
  description = "Stage (e.g. `prod`, `dev`, `staging`"
}

variable "delimiter" {
  default     = "-"
  description = "Delimiter to be used between `name`, `namespace`, `stage`, etc."
}

variable "attributes" {
  description = "Additional attributes (e.g. `1`)"
  type        = list(any)
  default     = []
}

variable "availability_zones" {
  description = "AWS Availablity Zones"
  type      = list(any)
  default   = ["us-east-1a","us-east-1b","us-east-1c"]
}

variable "tags" {
  description = "Additional tags"
  type        = map(any)
  default     = {}
}

#Redis Variables

variable "cluster_size" {
  type        = string
  description = "cluster_size"
}

variable "redis_instance_type" {
  type        = string
  description = "redis instance_type"
}


variable "redis_engine_version" {
  type        = string
  description = "redis engine_version"
}

variable "redis_family" {
  type        = string
  description = "Redis Family"
}

variable "redis_port" {
  type        = string
  description = "Redis port"
}