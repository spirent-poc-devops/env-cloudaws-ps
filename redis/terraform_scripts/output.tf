#Elastic Cache Redis Outputs
output "endpoint" {
  description = "List of public IP addresses assigned to the instances, if applicable"
  value       = module.redis.endpoint
}

output "id" {
  description = "List of public IP addresses assigned to the instances, if applicable"
  value       = module.redis.id
}

output "port" {
  description = "List of public IP addresses assigned to the instances, if applicable"
  value       = module.redis.port
}
output "redis_security_group_id" {
  description = "List of public IP addresses assigned to the instances, if applicable"
  value       = module.redis.security_group_id
}
