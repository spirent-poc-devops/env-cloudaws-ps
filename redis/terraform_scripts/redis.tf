module "label" {
  source = "cloudposse/label/null"
  version = "0.24.1"
  namespace  = var.namespace
  stage      = var.stage
  name       = var.env_nameprefix
  attributes = ["mgmtstation"]
  delimiter  = "-"

  tags = {
    configprefix       = var.env_nameprefix
  }
}
  
  module "redis" {
    source                     = "cloudposse/elasticache-redis/aws"
    version                    = "0.38.0"
    availability_zones         = var.availability_zones
    namespace                  = var.namespace
    stage                      = var.stage
    name                       = var.env_nameprefix
    attributes                 = ["redis"]
    vpc_id                     = var.vpc
    allowed_security_groups    = [ var.vpc_default_sg ]
    subnets                    = var.public_subnets
    cluster_size               = var.cluster_size
    instance_type              = var.redis_instance_type
    enabled                    = true
    apply_immediately          = true
    automatic_failover_enabled = true
    port                       = var.redis_port
    multi_az_enabled           = true
    engine_version             = var.redis_engine_version
    family                     = var.redis_family
    at_rest_encryption_enabled = false
    transit_encryption_enabled = false
    tags                       = module.label.tags

    parameter = [
      {
        name  = "notify-keyspace-events"
        value = "lK"
      }
    ]
  }