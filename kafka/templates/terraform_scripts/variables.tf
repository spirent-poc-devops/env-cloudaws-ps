variable "aws_region" {
  type        = string
  description = "AWS Region"
}

variable "aws_credentials_file" {
  type        = string
  description = "AWS Credentials File"
}

variable "aws_profile" {
  type        = string
  description = "AWS Profile"
}

variable "ssh_key_pair_path" {
  description = "Path to where the generated key pairs will be created. Defaults to $${path.cwd}"
  default     = "./secrets"
}

variable "account_id" {
  description = "Account ID of the AWS account"
}

variable "vpc" {
  type        = string
  description = "AWS VPC"
}

variable "private_subnets" {
  type        = list
  description = "AWS Public Subnets"
}

variable "namespace" {
  description = "Namespace (e.g. `cp` or `cloudposse`)"
}

variable "stage" {
  description = "Stage (e.g. `prod`, `dev`, `staging`"
}

variable "delimiter" {
  default     = "-"
  description = "Delimiter to be used between `name`, `namespace`, `stage`, etc."
}

variable "attributes" {
  description = "Additional attributes (e.g. `1`)"
  type        = list(any)
  default     = []
}

variable "tags" {
  description = "Additional tags"
  type        = map(any)
  default     = {}
}

#MSK Variables

variable "env_nameprefix" {
  type        = string
  description = "Name Prefix"
}

variable "s3_bucket_name"{
    description = "S3 bucket Name"
}

variable "config_prefix" {
 description = "Resource Config Prefix"
}

variable "allowed_cidr_blocks_cluster" {
  type        = list(any)
  default     = ["172.31.0.0/16"]
  description = "List of CIDR blocks to be allowed to connect to the EKS cluster"
}

variable "kafka_version" {
  type        = string
  description = "AWS MSK Kafka Version"
}

variable "number_of_kafka_broker_nodes" {
  type        = string
  description = "Number of Kafka Brokers"
}
variable "kafka_broker_instance_type" {
  type        = string
  description = "Kafka Broker Instance Type"
}
variable "kafka_broker_volume_size" {
  type        = string
  description = "Kafka Broker Volume Size"
}
variable "aws_zone_id" {
  type        = string
  description = "AWS Hosted Zone ID"
}
variable "encryption_in_transit" {
  type        = string
  description = "Encryption setting for data in transit between clients and brokers. Valid values: TLS, TLS_PLAINTEXT, and PLAINTEXT"
}
variable "kafka_port" {
  type        = string
  description = "preferred kafka port"
}
