data "terraform_remote_state" "eks" {
  backend = "s3"

  config = {
    bucket = var.s3_bucket_name
    key = "${var.env_nameprefix}/${var.config_prefix}/${var.namespace}"
    region = var.aws_region
  }
}

module "label" {
  source     = "cloudposse/label/null"
  version    = "0.24.1"
  namespace  = var.namespace
  name       = var.env_nameprefix
  stage      = var.stage
  delimiter  = var.delimiter
  attributes = compact(concat(var.attributes, list("msk-cluster")))

  tags = {
    configprefix       = var.env_nameprefix
  }
}

module "kafka" {
  source                 = "git::https://github.com/cloudposse/terraform-aws-msk-apache-kafka-cluster.git?ref=0.5.2"
  namespace              = var.namespace
  name                   = var.env_nameprefix
  attributes             = ["msk"]
  stage                  = var.stage
  delimiter              = var.delimiter
  vpc_id                 = var.vpc
  enabled                = true
  security_groups        = [data.terraform_remote_state.eks.outputs.workers_security_group_id, data.terraform_remote_state.eks.outputs.eks_cluster_security_group]
  subnet_ids             = var.private_subnets
  kafka_version          = var.kafka_version
  client_broker          = var.encryption_in_transit
  number_of_broker_nodes = var.number_of_kafka_broker_nodes
  broker_instance_type   = var.kafka_broker_instance_type
  broker_volume_size     = var.kafka_broker_volume_size
  zone_id                = var.aws_zone_id
  allowed_cidr_blocks    = var.allowed_cidr_blocks_cluster
  tags                   = module.label.tags
  properties = {
    "auto.create.topics.enable" = true
    "compression.type" = "uncompressed"
    "default.replication.factor" = 2
    "delete.topic.enable" = true
    "log.retention.hours" = 24
    "log.cleanup.policy" = "delete"
    "num.partitions" = 1
    "message.max.bytes" = 10485760
    }
}