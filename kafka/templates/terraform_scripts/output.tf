#EKS Cluster Outputs
output "cluster_name" {
  description = "MSK Cluster Name"
  value       = module.kafka.cluster_name
}
output "cluster_arn" {
  description = "MSK Cluster Arn"
  value       = module.kafka.cluster_arn
}
output "bootstrap_brokers_tls"{
  description = "MSK Cluster bootstrap broker TLS"
  value       = module.kafka.bootstrap_broker_tls
}
output "bootstrap_brokers"{
  description = "MSK Cluster bootstrap brokers"
  value       = module.kafka.bootstrap_brokers
}
output "hostname" {
  description = "MSK Cluster Broker DNS hostname"
  value = module.kafka.hostname
}