#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [hashtable] $Config,

    [Parameter(Mandatory = $true, Position = 1)]
    [hashtable] $Resources
)

$kafkaTerraformPath = "$PSScriptRoot/../temp/kafka_$(Get-EnvMapValue -Map $config -Key "environment.prefix")"
if (!(Test-Path $kafkaTerraformPath)) {
    $null = New-Item $kafkaTerraformPath -ItemType "directory"
}

$env:TF_VAR_aws_region = Get-EnvMapValue -Map $config -Key "aws.region"
$env:TF_VAR_aws_credentials_file = Get-EnvMapValue -Map $config -Key "aws.credentials_file"
$env:TF_VAR_aws_profile = Get-EnvMapValue -Map $config -Key "aws.profile"
$env:TF_VAR_account_id = Get-EnvMapValue -Map $config -Key "aws.account_id"
$env:TF_VAR_namespace = Get-EnvMapValue -Map $config -Key "aws.namespace"
$env:TF_VAR_stage = Get-EnvMapValue -Map $config -Key "aws.stage"
$env:TF_VAR_nenv_nameprefix = Get-EnvMapValue -Map $config -Key "environment.prefix"
$env:TF_VAR_aws_zone_id  = Get-EnvMapValue -Map $config -Key "aws.zone_id"
$env:TF_VAR_vpc = Get-EnvMapValue -Map $config -Key "aws.vpc"

#subnets 
if((Get-EnvMapValue -Map $config -Key "aws.private_subnets").count -eq 0 -or $null -eq (Get-EnvMapValue -Map $config -Key "aws.private_subnets"))
{
    Write-EnvError -Component "kafka" "AWS subnets array config cannot be empty."
}
$privateSubnets = (Get-EnvMapValue -Map $config -Key "aws.private_subnets") -join '","'
$privateSubnets = '["' + $privateSubnets + '"]'
$env:TF_VAR_private_subnets = $privateSubnets

#MSK Terraform Variables
$env:TF_VAR_eks_worker_security_group_ids = Get-EnvMapValue -Map $resources -Key "kafka.workers_security_group_id"
$env:TF_VAR_kafka_version = Get-EnvMapValue -Map $config -Key "kafka.version"
$env:TF_VAR_number_of_kafka_broker_nodes = Get-EnvMapValue -Map $config -Key "kafka.broker_nodes_count"
$env:TF_VAR_kafka_broker_instance_type = Get-EnvMapValue -Map $config -Key "kafka.broker_instance_type"
$env:TF_VAR_kafka_broker_volume_size = Get-EnvMapValue -Map $config -Key "kafka.broker_volume_size"
$env:TF_VAR_encryption_in_transit = Get-EnvMapValue -Map $config -Key "kafka.encryption_in_transit"
$env:TF_VAR_kms_key_id = Get-EnvMapValue -Map $config -Key "aws.kms_key_id"
$env:TF_VAR_config_prefix = "kafka"
$env:TF_VAR_kafka_port = Get-EnvMapValue -Map $config -Key "kafka.port"

Copy-Item -Path "$PSScriptRoot/templates/terraform_scripts/main.tf" -Destination "$kafkaTerraformPath/main.tf"
Copy-Item -Path "$PSScriptRoot/templates/terraform_scripts/msk.tf" -Destination "$kafkaTerraformPath/msk.tf"
Copy-Item -Path "$PSScriptRoot/templates/terraform_scripts/output.tf" -Destination "$kafkaTerraformPath/output.tf"
Copy-Item -Path "$PSScriptRoot/templates/terraform_scripts/variables.tf" -Destination "$kafkaTerraformPath/variables.tf"

# Load terraform state from config folder
Sync-State -Component "kafka" -EnvPrefix $(Get-EnvMapValue -Map $config -Key "environment.prefix")
