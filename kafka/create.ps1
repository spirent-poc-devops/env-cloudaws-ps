#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [string] $ConfigPath,

    [Parameter(Mandatory = $true, Position = 1)]
    [string] $ResourcePath
)

# Read config and resources
$config = Read-EnvConfig -ConfigPath $ConfigPath
$resources = Read-EnvResources -ResourcePath $ResourcePath

# Prepare terraform files and set terraform env vars
. "$PSScriptRoot/terraform.ps1" -Config $config -Resources $resources

Set-Location $kafkaTerraformPath
Write-Host "Initializing terraform..."
terraform init
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "kafka" "Can't initialize terraform. Watch logs above or check '$kafkaTerraformPath' folder content."
}

# Plan cloud resources creation
Write-Host "Executing terraform plan command..."
terraform plan
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "kafka" "Can't execute terraform plan. Watch logs above or check '$kafkaTerraformPath' folder content."
}

# Create cloud resources
Write-Host "Creating cloud resources by terraform..."
terraform apply -auto-approve
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "kafka" "Can't create cloud resources. Watch logs above or check '$kafkaTerraformPath' folder content."
}

# MSK Cluster Outputs
$mskClusterName=$(terraform output cluster_name).Trim('"')
$mskClusterArn=$(terraform output cluster_arn).Trim('"')
$mskBootstrapBrokers=$(terraform output bootstrap_brokers).Trim('"')
$mskHostName=$(terraform output hostname).Trim('"')

#Adding Resources to Resource file
Set-EnvMapValue -Map $resources -Key "kafka.cluster_name" -Value $mskClusterName
Set-EnvMapValue -Map $resources -Key "kafka.cluster_arn" -Value  $mskClusterArn
Set-EnvMapValue -Map $resources -Key "kafka.host" -Value  $mskBootstrapBrokers.split(",")[0].split(":")[0]
Set-EnvMapValue -Map $resources -Key "kafka.hosts" -Value  $mskBootstrapBrokers
Set-EnvMapValue -Map $resources -Key "kafka.port" -Value  $(Get-EnvMapValue -Map $config -Key "kafka.port")
Set-EnvMapValue -Map $resources -Key "kafka.hostname" -Value  $mskHostName

Set-Location -Path "$PSScriptRoot/.."

# Save terraform state to config folder
Save-State -Component "kafka" -EnvPrefix $(Get-EnvMapValue -Map $config -Key "environment.prefix")

Write-EnvResources -ResourcePath $ResourcePath -Resources $resources
