output "public_ip" {
  description = "List of public IP addresses assigned to the instances, if applicable"
  value       = module.mgmt_station_instance.*.public_ips
}

output "private_ip" {
  description = "List of private IP addresses assigned to the instances, if applicable"
  value       = module.mgmt_station_instance.*.private_ips
}