#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [hashtable] $Config,

    [Parameter(Mandatory = $true, Position = 1)]
    [hashtable] $Resources
)

$env:TF_VAR_env_prefix = Get-EnvMapValue -Map $config -Key "environment.prefix"
$env:TF_VAR_aws_region = Get-EnvMapValue -Map $config -Key "aws.region"
$env:TF_VAR_aws_credentials_file = Get-EnvMapValue -Map $config -Key "aws.credentials_file"
$env:TF_VAR_aws_profile = Get-EnvMapValue -Map $config -Key "aws.profile"
$env:TF_VAR_vpc = Get-EnvMapValue -Map $config -Key "aws.vpc"
$env:TF_VAR_account_id = Get-EnvMapValue -Map $config -Key "aws.account_id"
$env:TF_VAR_namespace = Get-EnvMapValue -Map $config -Key "aws.namespace"
$env:TF_VAR_stage = Get-EnvMapValue -Map $config -Key "aws.stage"
$env:TF_VAR_stage_short = Get-EnvMapValue -Map $config -Key "aws.stage_short"
$env:TF_VAR_terraform_role_name = Get-EnvMapValue -Map $config -Key "aws.terraform_role_name"
$env:TF_VAR_ami = Get-EnvMapValue -Map $config -Key "mgmtstation.instance_ami"
$env:TF_VAR_ami_owner = Get-EnvMapValue -Map $config -Key "mgmtstation.ami_owner"
$env:TF_VAR_subnet = Get-EnvMapValue -Map $config -Key "mgmtstation.subnet"
$env:TF_VAR_sg_id = Get-EnvMapValue -Map $config -Key "mgmtstation.sg_id"
$env:TF_VAR_instance_type = Get-EnvMapValue -Map $config -Key "mgmtstation.instance_type"
$env:TF_VAR_root_volume_size = Get-EnvMapValue -Map $config -Key "mgmtstation.root_volume_size"

$terraformPath = "$PSScriptRoot/../temp/mgmt_$(Get-EnvMapValue -Map $config -Key "environment.prefix")"
$envPrefix = Get-EnvMapValue -Map $config -Key "environment.prefix"
# Create management station
$terraformPath = "$PSScriptRoot/../temp/mgmt_$envPrefix"

if (!(Test-Path "$terraformPath")) {
    $null = New-Item "$terraformPath" -ItemType "directory"
} 

# Load terraform state from config folder
Sync-State -Component "mgmtstation" -EnvPrefix $envPrefix

Copy-Item -Path "$PSScriptRoot/terraform_scripts/*.tf" -Destination $terraformPath
