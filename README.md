# <img src="https://upload.wikimedia.org/wikipedia/en/thumb/c/ce/Spirent_logo.svg/490px-Spirent_logo.svg.png" alt="Spirent Logo" width="200"> Spirent VisionWorks Cloud Environment

This is the environment automation scripts to create, update and delete Cloud Environments.

The environment consists of the following components:

* **Kubernetes** cluster
* **Timescale** database
* **Neo4j** database
* **Kafka** message broker
* **Redis** distributed cache
* **ElasticSearch + Kibana** consolidated logging
* **Prometheus + Grafana** consolidated metrics

![Design](design.png)

Quick links:

* [Max Infrastructure Requirements](https://nowhere.com)
* [Max Infrastructure Architecture](https://nowhere.com)
* [Change Log](CHANGELOG.md)

## Use

Start the process from any computer. Download released package with environment provisioning scripts from [link...](http://somewhere.com/env-cloudaws-ps-1.0.0.zip)

```bash
wget http://somewhere.com/env-cloudaws-ps-1.0.0.zip
```

Unzip scripts from the package

```bash
unzip env-cloudaws-ps-1.0.0.zip
```

Go to the `config` folder and create configuration for a new environment.
Read configuration section below, copy [default_config.json](config/default_config.json) and set required values.

Your configuration may look like the sample below.

```json
{
    "env.type": "sample",
    "component.message_1": "Hello, world!"
}
```

Install prerequisites on your computer. The system should have terraform 14 or higher

Logging into AWS account through aws roles
1. User should be logged to the appropriate account role through `aws-azure-login`
2. Once the user is authenticated provide export the AWS Profile
```bash
export AWS_PROFILE=<profile_name>
```
Note: Profile name should be found at `~/.aws/config`

Create a management station, that will be used to create new environments.

```bash
create_resources.ps1 -Config <path to config file>
```

After you run the script you shall see a resource file (ending with `resources.json`) next to the config file you created.

Connect to the newly created management station (see it's address and credentials in the script output).
Install the environment scripts following the steps above and copy into `config` folder on the management station
config and resource files from your local computer. **The following steps shall be performed from the management station**.

Create a new environment. The resources file will contain references to newly created resources.

```bash
create_env.ps1 -Config <path to config file>
```

When environment configuration changes, you can update it without loosing data or reinstalling applications.

```bash
update_env.ps1 -Config <path to config file>
```

Delete the environment when its no longer needed to free up used computing resources.

```bash
delete_env.ps1 -Config <path to config file>
```

## Configuration

The environment configuration supports the following configuration parameters:

Parameter | Type| Default | Description
--------- | --- | ------- | ---------
env.type | string | sample | Type of the created environment

Configuration parameters for the sample component:
|Parameter|Type|Default|Description|
|--- |--- |--- |--- |
|component.message_1|string|Default message 1|A message for the first output|
|component.message_2|string|Default message 2|A message for the second output|
<!-- Todo -->

Example environment config file:
```json
{
    "environment": {
        "type"                          : "cloudaws",
        "prefix"                        : "testdemo",
        "version"                       : "1.0.0"
    },
    "aws": {
        "account_id"                    : "072434860318",
        "vpc"                           : "vpc-09e59cb6c44b1c5c4",
        "public_subnets"                : ["subnet-028bb6b6d2ca7342e", "subnet-0c637d6329c805b3b", "subnet-0c331448bb9cb9912"],
        "private_subnets"               : ["subnet-0cc94ff526fab52b4","subnet-0a943b8f773a8b127","subnet-08e5e953271c10fe3"],
        "region"                        : "us-east-1", 
        "namespace"                     : "sbx006",
        "stage"                         : "sandbox",
        "zone_id"                       : "Z0880600MVJB229J5CWW",
        "subdomain"                     : ".sbx006.spirentcds.io",
        "vpc_default_sg"                : "sg-0ad956e2f350f92e7"
    },
    "mgmtstation": {
        "instance_ami"                  : "ami-08cc10a3eebe46341",
        "instance_type"                 : "t3.medium",
        "root_volume_size"              : "50",
        "instance_username"             : "ubuntu",
    },
    "copy_project_to_mgmt_station"      : true,
    "eks": {
        "version"                       : "1.19",
        "worker_instance_type"          : "t3.medium",
        "workers_max_size"              : "3",
        "workers_min_size"              : "2",
        "worker_cpu_utilization_high_threshold_percent" : "80",
        "worker_cpu_utilization_low_threshold_percent"  :"5",
        "namespace": "infra"
    },
    "msk": {
        "version"                     : "2.7.0",
        "broker_nodes_count"          : "3",
        "broker_instance_type"        : "kafka.m5.large",
        "broker_volume_size"          : "50"
    },
    "redis": {
        "cluster_size"                      : "2",
        "instance_type"                     : "cache.t3.small",
        "engine_version"                    : "5.0.6",
        "family"                            : "redis5.0",
        "port"                              : "6379"
    },
    "docker" : { 
        "registry" : "ghcr.io", 
        "username" : "XXXXX", 
        "password" : "XXXX", 
        "email" : "XXXX.XXXX@spirent.com" 
    }
}
```

## Resources

As the result of the scripts information about created resources will be saved into a resource file that is ended with `resources.json`
and placed next to the configuration file used to create an environment.

Resource parameters saved for the sample component:
|Parameter|Type|Description|
|--- |--- |--- |
|component.output_1|string|The first output|
|component.output_2|string|The second output|
<!-- Todo -->

Example environment resources file:
```json
{
  "eks": {
    "worker_instance_type": "t3.medium",
    "worker_cpu_utilization_low_threshold_percent": "5",
    "cluster_name": "sbx006-sandbox-testdemo-cluster",
    "worker_autoscaling_min_size": "2",
    "workers_autoscaling_group_name": "sbx006-sandbox-testdemo-20210323032814881700000009",
    "cluster_arn": "arn:aws:eks:us-east-1:072434860318:cluster/sbx006-sandbox-testdemo-cluster",
    "worker_autoscaling_max_size": "3",
    "workers_security_group_name": "sbx006-sandbox-testdemo-workers",
    "workers_security_group_arn": "arn:aws:ec2:us-east-1:072434860318:security-group/sg-0708ef66ac96b621d",
    "workers_security_group_id": "sg-0708ef66ac96b621d",
    "workers_role_arn": "arn:aws:iam::072434860318:role/sbx006-sandbox-testdemo-workers",
    "worker_cpu_utilization_high_threshold_percent": "80",
    "cluster_role_arn": "arn:aws:iam::072434860318:role/sbx006-sandbox-testdemo-cluster",
    "cluster_endpoint": "https://EF7169C43454CDB092A57C17B22ABF4D.gr7.us-east-1.eks.amazonaws.com",
    "workers_autoscaling_group_id": "sbx006-sandbox-testdemo-20210323032814881700000009",
    "version": "1.19",
    "worker_autoscaling_group_arn": "arn:aws:autoscaling:us-east-1:072434860318:autoScalingGroup:13818ace-b1ab-47b1-89f1-6818858e8bee:autoScalingGroupName/sbx006-sandbox-testdemo-20210323032814881700000009",
    "workers_name": "sbx006-sandbox-testdemo-workers"
  },
  "mgmtstation": {
    "keypair_name": "sbx006-sandbox-demotest-mgmt-station",
    "instance_name": "sbx006-sandbox-demotest-mgmt-station",
    "public_ip": "3.80.41.239",
    "envprefix": "demotest"
  },
  "redis": {
    "security_group_id": "sg-097eea4b7f45aef86",
    "name": "sbx006-sandbox-testdemo-redis",
    "port": "6379",
    "endpoint": "sbx006-sandbox-testdemo-redis.tu57be.ng.0001.use1.cache.amazonaws.com"
  },
  "environment": {
    "version": "1.0.0",
    "type": "cloudaws",
    "create_time": "2021-03-24T18:30:21.6677552+00:00"
  },
  "s3": {
    "name": "s3://sbx006-sandbox-testdemo-bucket"
  },
  "msk": {
    "cluster_arn": "arn:aws:kafka:us-east-1:072434860318:cluster/sbx006-sandbox-testdemo-msk/02f47eee-d887-4f0f-b332-77ec3022cae7-11",
    "cluster_name": "sbx006-sandbox-testdemo-msk"
  },
  "connectparams": {
    "status": "created"
  }
}
```

## Contacts

This environment was created and currently maintained by the team managed by *Sudheer Sangunni* .
