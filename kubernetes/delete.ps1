#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [string] $ConfigPath,

    [Parameter(Mandatory = $true, Position = 1)]
    [string] $ResourcePath
)

# Read config and resources
$config = Read-EnvConfig -ConfigPath $ConfigPath
$resources = Read-EnvResources -ResourcePath $ResourcePath

# Prepare terraform files and set terraform env vars
. "$PSScriptRoot/terraform.ps1" -Config $config -Resources $resources

# Delete aws kubernetes resources
Set-Location $kubernetesTerraformPath
terraform init
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "kubernetes" "Can't initialize terraform. Watch logs above or check '$kubernetesTerraformPath' folder content."
}

terraform plan
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "kubernetes" "Can't execute terraform plan. Watch logs above or check '$kubernetesTerraformPath' folder content."
}

terraform destroy -auto-approve
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "kubernetes" "Can't delete cloud resources. Watch logs above or check '$kubernetesTerraformPath' folder content."
}

Set-Location "$PSScriptRoot/.."

# Delete terraform state from config folder
Remove-State -Component "kubernetes" -EnvPrefix $(Get-EnvMapValue -Map $config -Key "environment.prefix")

if (Test-EnvMapValue -Map $resources -Key "kubernetes") {
    Remove-EnvMapValue -Map $resources -Key "kubernetes.cluster_name"
    Remove-EnvMapValue -Map $resources -Key "kubernetes.version"
    Remove-EnvMapValue -Map $resources -Key "kubernetes.cluster_arn"
    Remove-EnvMapValue -Map $resources -Key "kubernetes.cluster_endpoint"
    Remove-EnvMapValue -Map $resources -Key "kubernetes.cluster_role_arn"
    Remove-EnvMapValue -Map $resources -Key "kubernetes.eks_cluster_identity_oidc_issuer_arn"
    Remove-EnvMapValue -Map $resources -Key "kubernetes.eks_cluster_identity_oidc_issuer"

    Remove-EnvMapValue -Map $resources -Key "kubernetes.workers_name"
    Remove-EnvMapValue -Map $resources -Key "kubernetes.workers_role_arn"
    Remove-EnvMapValue -Map $resources -Key "kubernetes.worker_instance_type"
    Remove-EnvMapValue -Map $resources -Key "kubernetes.worker_autoscaling_max_size"
    Remove-EnvMapValue -Map $resources -Key "kubernetes.worker_autoscaling_min_size"
    Remove-EnvMapValue -Map $resources -Key "kubernetes.worker_cpu_utilization_high_threshold_percent" 
    Remove-EnvMapValue -Map $resources -Key "kubernetes.worker_cpu_utilization_low_threshold_percent" 
    Remove-EnvMapValue -Map $resources -Key "kubernetes.worker_autoscaling_group_arn"
    Remove-EnvMapValue -Map $resources -Key "kubernetes.workers_autoscaling_group_id"
    Remove-EnvMapValue -Map $resources -Key "kubernetes.workers_autoscaling_group_name"
    Remove-EnvMapValue -Map $resources -Key "kubernetes.workers_security_group_id"
    Remove-EnvMapValue -Map $resources -Key "kubernetes.workers_security_group_arn"
    Remove-EnvMapValue -Map $resources -Key "kubernetes.workers_security_group_name"
    Remove-EnvMapValue -Map $resources -Key "kubernetes.eks_cluster_external_dns_ARN"
}

Write-EnvResources -ResourcePath $ResourcePath -Resources $resources
