#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [string] $ConfigPath,

    [Parameter(Mandatory = $true, Position = 1)]
    [string] $ResourcePath
)

# Read config and resources
$config = Read-EnvConfig -ConfigPath $ConfigPath
$resources = Read-EnvResources -ResourcePath $ResourcePath

# Prepare terraform files and set terraform env vars
. "$PSScriptRoot/terraform.ps1" -Config $config -Resources $resources

# Initialize terraform
Set-Location $kubernetesTerraformPath
Write-Host "Initializing terraform..."
terraform init
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "kubernetes" "Can't initialize terraform. Watch logs above or check '$kubernetesTerraformPath' folder content."
}

# Plan cloud resources creation
Write-Host "Executing terraform plan command..."
terraform plan
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "kubernetes" "Can't execute terraform plan. Watch logs above or check '$kubernetesTerraformPath' folder content."
}

# Create cloud resources
Write-Host "Creating cloud resources by terraform..."
terraform apply -auto-approve
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "kubernetes" "Can't create cloud resources. Watch logs above or check '$kubernetesTerraformPath' folder content."
}

#EKS Cluster Outputs
$EKSClusterID=$(terraform output eks_cluster_id).Trim('"')
$EKSClusterArn=$(terraform output eks_cluster_arn).Trim('"')
$EKSClusterEndpoint=$(terraform output eks_cluster_endpoint).Trim('"')
$EKSClusterRoleArn=$(terraform output eks_cluster_role_arn).Trim('"')
$EKSClusterVersion=$(terraform output eks_cluster_version).Trim('"')
$EKSClusterOidcIssuer=$(terraform output eks_cluster_identity_oidc_issuer).Trim('"')
$EKSOidcURL = $EKSClusterOidcIssuer.Replace("https://", "")
$EKSClusterOidcIssuerARN=$(terraform output eks_cluster_identity_oidc_issuer_arn).Trim('"')
#EKS Worker Outputs
$autoscalingGroupArn=$(terraform output autoscaling_group_arn).Trim('"')
$autoscalingGroupId=$(terraform output autoscaling_group_id).Trim('"')
$autoscalingGroupName=$(terraform output autoscaling_group_name).Trim('"')
$workerArn=$(terraform output workers_role_arn).Trim('"')
$workerName=$(terraform output workers_role_name).Trim('"')
$sgArn=$(terraform output workers_security_group_arn).Trim('"')
$sgId=$(terraform output workers_security_group_id).Trim('"')
$sgName=$(terraform output workers_security_group_name).Trim('"')

Set-Location -Path "$PSScriptRoot/.."

# TODO
# # Tagging Private Subnets with EKS Cluster name for Istio Usage
# aws ec2 create-tags --region (Get-EnvMapValue -Map $config -Key "aws.region") --resources (Get-EnvMapValue -Map $config -Key "aws.private1") (Get-EnvMapValue -Map $config -Key "aws.private2") (Get-EnvMapValue -Map $config -Key "aws.private3") --tags "Key='kubernetes.io/cluster/$($EKSClusterID)',Value=shared" "Key='kubernetes.io/role/internal-elb',Value=1"
# Write-Host "Tags are updated to autodiscover the subnets for istio installation" -ForegroundColor Yellow

#EKS Cluster Details
Set-EnvMapValue -Map $resources -Key "kubernetes.cluster_name" -Value $EKSClusterID
Set-EnvMapValue -Map $resources -Key "kubernetes.version" -Value $EKSClusterVersion
Set-EnvMapValue -Map $resources -Key "kubernetes.cluster_arn" -Value $EKSClusterArn
Set-EnvMapValue -Map $resources -Key "kubernetes.cluster_endpoint" -Value $EKSClusterEndpoint
Set-EnvMapValue -Map $resources -Key "kubernetes.cluster_role_arn" -Value $EKSClusterRoleArn
Set-EnvMapValue -Map $resources -Key "kubernetes.eks_cluster_identity_oidc_issuer_arn" -Value $EKSClusterOidcIssuerARN
Set-EnvMapValue -Map $resources -Key "kubernetes.eks_cluster_identity_oidc_issuer" -Value $EKSOidcURL

#EKS Worker Details
Set-EnvMapValue -Map $resources -Key "kubernetes.workers_name" -Value $workerName
Set-EnvMapValue -Map $resources -Key "kubernetes.workers_role_arn" -Value $workerArn
Set-EnvMapValue -Map $resources -Key "kubernetes.worker_autoscaling_group_arn" -Value $autoscalingGroupArn
Set-EnvMapValue -Map $resources -Key "kubernetes.workers_autoscaling_group_id" -Value $autoscalingGroupId
Set-EnvMapValue -Map $resources -Key "kubernetes.workers_autoscaling_group_name" -Value $autoscalingGroupName
Set-EnvMapValue -Map $resources -Key "kubernetes.workers_security_group_arn" -Value $sgArn
Set-EnvMapValue -Map $resources -Key "kubernetes.workers_security_group_id" -Value $sgId
Set-EnvMapValue -Map $resources -Key "kubernetes.workers_security_group_name" -Value $sgName
Set-EnvMapValue -Map $resources -Key "kubernetes.worker_instance_type" -Value (Get-EnvMapValue -Map $config -Key "kubernetes.worker_instance_type")
Set-EnvMapValue -Map $resources -Key "kubernetes.worker_autoscaling_max_size" -Value (Get-EnvMapValue -Map $config -Key "kubernetes.workers_max_size")
Set-EnvMapValue -Map $resources -Key "kubernetes.worker_autoscaling_min_size" -Value (Get-EnvMapValue -Map $config -Key "kubernetes.workers_min_size")
Set-EnvMapValue -Map $resources -Key "kubernetes.worker_cpu_utilization_high_threshold_percent" -Value (Get-EnvMapValue -Map $config -Key "kubernetes.worker_cpu_utilization_high_threshold_percent")
Set-EnvMapValue -Map $resources -Key "kubernetes.worker_cpu_utilization_low_threshold_percent" -Value (Get-EnvMapValue -Map $config -Key "kubernetes.worker_cpu_utilization_low_threshold_percent")

# Save terraform state to config folder
Save-State -Component "kubernetes" -EnvPrefix $(Get-EnvMapValue -Map $config -Key "environment.prefix")

Write-EnvResources -ResourcePath $ResourcePath -Resources $resources

Write-Host "Waiting for a minutes for EKS worker nodes to get ready..." -ForegroundColor Green
Start-Sleep -Seconds 60

aws eks --region (Get-EnvMapValue -Map $config -Key "aws.region")  update-kubeconfig --name $EKSClusterID
# if ($LastExitCode -ne 0) {
#     Remove-Item -Path ~/.kube/config -Force
#     aws eks --region (Get-EnvMapValue -Map $config -Key "aws.region")  update-kubeconfig --name $EKSClusterID
#     }
# if ($LastExitCode -eq 0) {
#     Write-Host "Check path" $PSScriptRoot
#     chmod go-r ~/.kube/config
#     Set-Location -Path $PSScriptRoot/templates/kubernetes_manifests/
#     Write-Host "******Downloading aws-auth-cm*****" -ForegroundColor Green
#     curl -o aws-auth-cm.yaml https://s3.us-west-2.amazonaws.com/amazon-eks/cloudformation/2020-10-29/aws-auth-cm.yaml
#     $FileAwsAuthCM = "$PSScriptRoot/templates/kubernetes_manifests/aws-auth-cm.yaml"
#     $FindWorkerRoleArn = "- rolearn: <ARN of instance role (not instance profile)>"
#     $ReplaceWorkerRoleArn = "- rolearn: $workerArn"
#     (Get-Content $FileAwsAuthCM).replace($FindWorkerRoleArn, $ReplaceWorkerRoleArn) | Set-Content $FileAwsAuthCM
#     Write-Host "Applying aws-auth-cm" (kubectl apply -f aws-auth-cm.yaml)

#     $file = "$PSScriptRoot/templates/kubernetes_manifests/cluster-autoscaler-autodiscover.yaml"
#     $find = "--node-group-auto-discovery=asg:tag=k8s.io/cluster-autoscaler/enabled,k8s.io/cluster-autoscaler/<YOUR CLUSTER NAME>"
#     $replace = "--node-group-auto-discovery=asg:tag=k8s.io/cluster-autoscaler/enabled,k8s.io/cluster-autoscaler/$EKSClusterID"
#     (Get-Content $file).replace($find, $replace) | Set-Content $file
#     Write-Host "Apply cluster-autoscaler" (kubectl apply -f cluster-autoscaler-autodiscover.yaml)
#     Write-Host "Apply Metrics Server" (kubectl apply -f  metrics-server.yaml)

#     Set-Location -Path "$PSScriptRoot/.."
#     Write-Host "waiting for a minutes for pods to get ready......." -ForegroundColor Green
#     Start-Sleep -Seconds 60

#     Write-Host "*****EKS Component Statuses******"
#     kubectl get all -A
#     kubectl get nodes

#     Write-Host "*****EKS Component Statuses******"
#     kubectl get all -A
#     kubectl get nodes
        
#     $status = "CREATION_COMPLETED"
#     Write-Host "###################################################################"
#     Write-Host "EKS Cluster and it's components are up & running" -ForegroundColor Green
#     Write-Host "###################################################################"

#     # Create namespace
#     kubectl create namespace $(Get-EnvMapValue -Map $config -Key "kubernetes.namespace") --dry-run=client -o yaml | kubectl apply -f -

#     # $namespace = "kube-system"
#     # $eksexternaldnsrole = Get-EnvMapValue -Map $resources -Key "kubernetes.eks_cluster_external_dns_ARN"
#     # $domainFilter = Get-EnvMapValue -Map $config -Key "kubernetes.domainfilter"
#     # $hostedZoneID = Get-EnvMapValue -Map $config -Key "aws.zone_id"
#     # # Set variables from config
#     # $templateParams = @{ 
#     #     namespace=$namespace; 
#     #     eksexternaldnsrole=$eksexternaldnsrole; 
#     #     domainfilter=$domainFilter;  
#     #     hostedzoneid=$hostedZoneID
#     # }
#     # # Set variables from config
#     # Build-EnvTemplate -InputPath "$($path)/templates/external_dns.yaml" -OutputPath "$($path)/../temp/external_dns.yaml" -Params1 $templateParams
#     # # Create connection config map
#     # kubectl apply -f "$($path)/../temp/external_dns.yaml"
#     }else{
#         Set-Location -Path "$PSScriptRoot/.."
#         Write-Host "Failed to login EKS cluster, Watch above logs" -ForegroundColor Red
#         exit 0
#     }

#     # Proceeding on creating OIDC Role to External Dns Route 53 entry
#     $env:TF_VAR_oidc_url = Get-EnvMapValue -Map $resources -Key "kubernetes.eks_cluster_identity_oidc_issuer"
#     $env:TF_VAR_eks_cluster_identity_oidc_issuer_arn = Get-EnvMapValue -Map $resources -Key "kubernetes.eks_cluster_identity_oidc_issuer_arn"
#     $env:TF_VAR_aws_region = Get-EnvMapValue -Map $config -Key "aws.region"
#     Set-Location -Path $PSScriptRoot/templates/terraform_scripts/oidc_iam_role
#     Remove-Item -Recurse -Force .terraform*
#     Write-Host "Initiating Terraform Init, plan, apply" -ForegroundColor Green; terraform init -backend-config="bucket=${env:TF_VAR_s3_bucket_name}" -backend-config="role_arn=${env:TF_VAR_mgmtstation_role_arn}" -backend-config="region=${env:TF_VAR_aws_region}" -backend-config="kms_key_id=${env:TF_VAR_kms_key_id}" -backend-config="key=${env:TF_VAR_env_nameprefix}_kubernetes_oidctfstate/${env:TF_VAR_namespace}" -backend-config="encrypt=true"; 
#     terraform plan; terraform apply -auto-approve;
#     $EKSexternalDNSroleARN=$(terraform output eks_cluster_oidc_role_arn).Trim('"')
#     Set-Location -Path "$PSScriptRoot/.."
#     Set-EnvMapValue -Map $resources -Key "kubernetes.eks_cluster_external_dns_ARN" -Value $EKSexternalDNSroleARN
#     Write-EnvResources -ResourcePath $ResourcePath -Resources $resources
