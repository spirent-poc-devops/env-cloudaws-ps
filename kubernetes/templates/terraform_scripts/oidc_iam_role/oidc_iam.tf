# data "terraform_remote_state" "eks" {
#  backend = "s3"

#    config = {
#      bucket = var.s3_bucket_name
#      key = "${var.env_nameprefix}/${var.config_prefix}/${var.namespace}"
#      region = var.aws_region
#    }
# }

resource "aws_iam_role" "external_dns_role" {
  name        = "${var.namespace}-${var.stage}-${var.env_nameprefix}-external-dns-role"
assume_role_policy =  jsonencode({
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Federated": "${var.eks_cluster_identity_oidc_issuer_arn}"
      },
      "Action": "sts:AssumeRoleWithWebIdentity",
      "Condition": {
        "StringEquals": {
          "${var.oidc_url}:aud":"sts.amazonaws.com"
        }
      }
    }
  ]
})
}
resource "aws_iam_role_policy_attachment" "external_dns_role" {
  role = aws_iam_role.external_dns_role.name
}