output "eks_cluster_oidc_role_arn" {
  description = "Output EKS Cluster OIDC ROLE"
  value       = aws_iam_role.external_dns_role.arn
}