variable "oidc_url" {
  type = string
  description = "OIDC URL without https"
}

variable "eks_cluster_identity_oidc_issuer_arn"{
 type = string
}

# variable "s3_bucket_name"{
#     description = "S3 bucket Name"
# }

variable "config_prefix" {
 description = "Resource Config Prefix"
}

variable "namespace" {
  description = "Namespace (e.g. `cp` or `cloudposse`)"
}

variable "stage" {
  description = "Stage (e.g. `prod`, `dev`, `staging`"
}
variable "env_nameprefix" {
  type        = string
  description = "Name Prefix"
}
variable "aws_region" {
  type        = string
  description = "AWS Region"
}
variable "account_id" {
  description = "Account ID of the AWS account"
}
