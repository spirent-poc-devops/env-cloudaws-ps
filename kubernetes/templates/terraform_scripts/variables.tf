
variable "aws_region" {
  type        = string
  description = "AWS Region"
}

variable "aws_credentials_file" {
  type        = string
  description = "AWS Credentials File"
}

variable "aws_profile" {
  type        = string
  description = "AWS Profile"
}

variable "vpc" {
  type        = string
  description = "AWS VPC"
}
variable "public_subnets" {
  type        = list
  description = "AWS private Subnets"
}

variable "private_subnets" {
  type        = list
  description = "AWS private Subnets"
}

variable "private_access_cidrs" {
  type        = list
  description = "List of cidr be allowed to connect to the worker nodes"
}
variable "enabled" {
  type        = string
  description = "Whether to create the resources. Set to `false` to prevent the module from creating any resources"
  default     = "true"
}

variable "env_nameprefix" {
  type        = string
  description = "Name prefix"
}

variable "ssh_key_pair_path" {
  description = "Path to where the generated key pairs will be created. Defaults to $${path.cwd}"
  default     = "./secrets"
}

variable "account_id" {
  description = "Account ID of the AWS account"
}

variable "kms_key_id"{
    description = "Terraform State Backend KMS KEY ID"
}

variable "namespace" {
  description = "Namespace (e.g. `cp` or `cloudposse`)"
}

variable "stage" {
  description = "Stage (e.g. `prod`, `dev`, `staging`"
}

variable "delimiter" {
  default     = "-"
  description = "Delimiter to be used between `name`, `namespace`, `stage`, etc."
}

variable "attributes" {
  description = "Additional attributes (e.g. `1`)"
  type        = list(any)
  default     = []
}

#kubernetes Variables

variable "kubernetes_version" {
  type        = string
  description = "EKS cluster Version"
}

variable "eks_worker_instance_type" {
  type        = string
  description = "EKS Worker Instance Type"
}
variable "health_check_type" {
  type        = string
  description = "Controls how health checking is done. Valid values are `EC2` or `ELB`"
  default     = "EC2"
}

variable "max_size" {
  description = "The maximum size of the AutoScaling Group"
}

variable "min_size" {
  description = "The minimum size of the AutoScaling Group"
}

variable "wait_for_capacity_timeout" {
  type        = string
  description = "A maximum duration that Terraform should wait for ASG instances to be healthy before timing out. Setting this to '0' causes Terraform to skip all Capacity Waiting behavior"
  default     = "10m"
}

variable "autoscaling_policies_enabled" {
  type        = string
  default     = "true"
  description = "Whether to create `aws_autoscaling_policy` and `aws_cloudwatch_metric_alarm` resources to control Auto Scaling"
}

variable "cpu_utilization_high_threshold_percent" {
  type        = string
  description = "Worker nodes AutoScaling Group CPU utilization high threshold percent"
}

variable "cpu_utilization_low_threshold_percent" {
  type        = string
  description = "Worker nodes AutoScaling Group CPU utilization low threshold percent"
}

variable "allowed_security_groups_workers" {
  type        = list(any)
  default     = []
  description = "List of Security Group IDs to be allowed to connect to the worker nodes"
}

variable "allowed_cidr_blocks_cluster" {
  type        = list(any)
  default     = ["172.31.0.0/16"]
  description = "List of CIDR blocks to be allowed to connect to the EKS cluster"
}

variable "allowed_cidr_blocks_workers" {
  type        = list(any)
  default     = ["172.31.0.0/16"]
  description = "List of CIDR blocks to be allowed to connect to the worker nodes"
}