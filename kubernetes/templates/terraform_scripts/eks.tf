module "label" {
  source     = "cloudposse/label/null"
  version    = "0.24.1"
  namespace  = var.namespace
  name       = var.env_nameprefix
  stage      = var.stage
  delimiter  = var.delimiter
  attributes = compact(concat(var.attributes, list("cluster")))
  tags = {
    configprefix       = var.env_nameprefix
  }
}

module "ssh_key_pair" {
  source                = "cloudposse/key-pair/aws"
  version               = "0.18.0"
  namespace             = var.namespace
  stage                 = var.stage
  name                  = var.env_nameprefix
  ssh_public_key_path   = var.ssh_key_pair_path
  attributes            = ["eksworker-keypair"]
  generate_ssh_key      = "true"
  private_key_extension = ".pem"
  public_key_extension  = ".pub"
  tags                  = module.label.tags
}

## EKS Cluster

module "eks_cluster" {
  source                     = "cloudposse/eks-cluster/aws"
  version                    = "0.38.0" 
  namespace                  = var.namespace
  stage                      = var.stage
  name                       = var.env_nameprefix
  region                     = var.aws_region
  attributes                 = var.attributes
  vpc_id                     = var.vpc
  subnet_ids                 = flatten([var.public_subnets, var.private_subnets])
  workers_security_group_ids = [module.eks_workers.security_group_id]
  workers_role_arns          = [module.eks_workers.workers_role_arn]
  kubernetes_version        = var.kubernetes_version
  apply_config_map_aws_auth = false
  oidc_provider_enabled     = true

  allowed_cidr_blocks = var.allowed_cidr_blocks_cluster
  enabled             = var.enabled
  tags                = module.label.tags
}

#EKS Worker

module "eks_workers" {
  source                             = "cloudposse/eks-workers/aws"
  version                            = "0.18.2"
  namespace                          = var.namespace
  stage                              = var.stage
  name                               = var.env_nameprefix
  attributes                         = var.attributes
  eks_worker_ami_name_filter         = "amazon-eks-node-${var.kubernetes_version}*"
  instance_type                      = var.eks_worker_instance_type
  vpc_id                             = var.vpc
  subnet_ids                         = var.private_subnets
  health_check_type                  = var.health_check_type
  min_size                           = var.min_size
  max_size                           = var.max_size
  key_name                           = module.ssh_key_pair.key_name
  wait_for_capacity_timeout          = var.wait_for_capacity_timeout
  associate_public_ip_address        = true
  cluster_name                       = module.label.id
  cluster_endpoint                   = module.eks_cluster.eks_cluster_endpoint
  cluster_certificate_authority_data = module.eks_cluster.eks_cluster_certificate_authority_data
  cluster_security_group_id          = module.eks_cluster.security_group_id
  allowed_security_groups            = var.allowed_security_groups_workers
  allowed_cidr_blocks                = var.private_access_cidrs
  enabled                            = true
  block_device_mappings = [
    {
      device_name  = "/dev/xvda"
      no_device    = "false"
      virtual_name = "root"
      ebs = {
        encrypted             = true
        volume_size           = 100
        delete_on_termination = true
        iops                  = null
        kms_key_id            = null
        snapshot_id           = null
        volume_type           = "standard"
      }
    }
  ]

  # Auto-scaling policies and CloudWatch metric alarms
  autoscaling_policies_enabled           = var.autoscaling_policies_enabled
  cpu_utilization_high_threshold_percent = var.cpu_utilization_high_threshold_percent
  cpu_utilization_low_threshold_percent  = var.cpu_utilization_low_threshold_percent
  
  #These tag is mandatory to connect eks workers to EKS cluster
  tags = merge(module.label.tags,  map("kubernetes.io/cluster/${module.label.id}", "owned"), map("k8s.io/cluster-autoscaler/enabled", true),map("kubernetes.io/cluster-autoscaler/${module.label.id}", "owned"))
}

resource "aws_iam_policy" "cluster_autoscaler_policy" {
  name        = "${var.namespace}-${var.stage}-${var.env_nameprefix}-eksca-policy"
  description = "EKS Cluster AutoScaler Policy"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "autoscaling:DescribeAutoScalingGroups",
                "autoscaling:DescribeAutoScalingInstances",
                "autoscaling:DescribeLaunchConfigurations",
                "autoscaling:DescribeTags",
                "autoscaling:SetDesiredCapacity",
                "autoscaling:TerminateInstanceInAutoScalingGroup",
                "ec2:DescribeLaunchTemplateVersions"
            ],
            "Resource": "*"
        }
    ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "cluster_autoscaler" {
  role       = module.eks_workers.workers_role_name
  policy_arn = aws_iam_policy.cluster_autoscaler_policy.arn
}