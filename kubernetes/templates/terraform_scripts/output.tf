#EKS Cluster Outputs
output "eks_cluster_id" {
  description = "Output EKS Cluster Id"
  value       = module.eks_cluster.eks_cluster_id
}

output "eks_cluster_arn" {
  description = "Output eks cluster arn"
  value       = module.eks_cluster.eks_cluster_arn
}

output "eks_cluster_endpoint" {
  description = "Output EKS cluster Endpoint"
  value       = module.eks_cluster.eks_cluster_endpoint
}

output "eks_cluster_role_arn" {
  description = "Output EKS Cluster Role Arn"
  value       = module.eks_cluster.eks_cluster_role_arn
}

output "eks_cluster_version" {
  description = "Output EKS Cluster Version"
  value       = module.eks_cluster.eks_cluster_version
}

output "eks_cluster_security_group" {
  description = "Output EKS Cluster Security Group"
  value       = module.eks_cluster.security_group_id
}

output "eks_cluster_identity_oidc_issuer" {
  description = "OIDC Provider Issuer"
  value      = module.eks_cluster.eks_cluster_identity_oidc_issuer
}

output "eks_cluster_identity_oidc_issuer_arn" {
  description = "OIDC Provider Issuer ARN"
  value      = module.eks_cluster.eks_cluster_identity_oidc_issuer_arn
}

#EKS Worker Outputs
output "autoscaling_group_arn" {
  description = "Output AutoScaling Group Arn"
  value       = module.eks_workers.autoscaling_group_arn
}

output "autoscaling_group_id" {
  description = "Output AutoScaling Group ID"
  value       = module.eks_workers.autoscaling_group_id
}

output "autoscaling_group_name" {
  description = "Output AutoScaling Group Name"
  value       = module.eks_workers.autoscaling_group_name
}

output "workers_role_arn" {
  description = "Output Workers Role Arn"
  value       = module.eks_workers.workers_role_arn
}

output "workers_role_name" {
  description = "Output worker Role Name"
  value       = module.eks_workers.workers_role_name
}

output "workers_security_group_name" {
  description = "Output Workers Security group Name"
  value       = module.eks_workers.security_group_name
}
output "workers_security_group_arn" {
  description = "Output Workers Security Group Arn"
  value       = module.eks_workers.security_group_arn
}
output "workers_security_group_id" {
  description = "Output Workers Security Group ID"
  value       = module.eks_workers.security_group_id
}

