#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [hashtable] $Config,

    [Parameter(Mandatory = $true, Position = 1)]
    [hashtable] $Resources
)

$kubernetesTerraformPath = "$PSScriptRoot/../temp/kubernetes_$(Get-EnvMapValue -Map $config -Key "environment.prefix")"
if (!(Test-Path $kubernetesTerraformPath)) {
    $null = New-Item $kubernetesTerraformPath -ItemType "directory"
}

$env:TF_VAR_aws_region = Get-EnvMapValue -Map $config -Key "aws.region"
$env:TF_VAR_aws_credentials_file = Get-EnvMapValue -Map $config -Key "aws.credentials_file"
$env:TF_VAR_aws_profile = Get-EnvMapValue -Map $config -Key "aws.profile"
$env:TF_VAR_account_id = Get-EnvMapValue -Map $config -Key "aws.account_id"
$env:TF_VAR_namespace = Get-EnvMapValue -Map $config -Key "aws.namespace"
$env:TF_VAR_stage = Get-EnvMapValue -Map $config -Key "aws.stage"
$env:TF_VAR_env_nameprefix = Get-EnvMapValue -Map $config -Key "environment.prefix"
$env:TF_VAR_vpc = Get-EnvMapValue -Map $config -Key "aws.vpc"

if((Get-EnvMapValue -Map $config -Key "aws.public_subnets").count -eq 0 -or $null -eq (Get-EnvMapValue -Map $config -Key "aws.public_subnets"))
{
    Write-EnvError -Component "kubernetes" "aws public subnets array config cannot be empty."
}
#subnets 
$publicSubnets = (Get-EnvMapValue -Map $config -Key "aws.public_subnets") -join '","'
$publicSubnets = '["' + $publicSubnets + '"]'
$env:TF_VAR_public_subnets = $publicSubnets

if((Get-EnvMapValue -Map $config -Key "aws.private_subnets").count -eq 0 -or $null -eq (Get-EnvMapValue -Map $config -Key "aws.private_subnets"))
{
    Write-EnvError -Component "kubernetes" "aws private subnets array config cannot be empty."
}
#subnets 
$privateSubnets = (Get-EnvMapValue -Map $config -Key "aws.private_subnets") -join '","'
$privateSubnets = '["' + $privateSubnets + '"]'
$env:TF_VAR_private_subnets = $privateSubnets

#private_access_cidrs
$privateaccesscidr = (Get-EnvMapValue -Map $config -Key "aws.private_access_cidrs") -join '","'
$privateaccesscidr = '["' + $privateaccesscidr + '"]'
$env:TF_VAR_private_access_cidrs = $privateaccesscidr

#Kubernetes Terraform Variables

$env:TF_VAR_kubernetes_version = Get-EnvMapValue -Map $config -Key "kubernetes.version"
$env:TF_VAR_eks_worker_instance_type = Get-EnvMapValue -Map $config -Key "kubernetes.worker_instance_type"
$env:TF_VAR_max_size = Get-EnvMapValue -Map $config -Key "kubernetes.workers_max_size"
$env:TF_VAR_min_size = Get-EnvMapValue -Map $config -Key "kubernetes.workers_min_size"
$env:TF_VAR_cpu_utilization_high_threshold_percent = Get-EnvMapValue -Map $config -Key "kubernetes.worker_cpu_utilization_high_threshold_percent"
$env:TF_VAR_cpu_utilization_low_threshold_percent = Get-EnvMapValue -Map $config -Key "kubernetes.worker_cpu_utilization_low_threshold_percent"
$env:TF_VAR_kms_key_id = Get-EnvMapValue -Map $config -Key "aws.kms_key_id"
$env:TF_VAR_config_prefix = "kubernetes"

Copy-Item -Path "$PSScriptRoot/templates/terraform_scripts/main.tf" -Destination "$kubernetesTerraformPath/main.tf"
Copy-Item -Path "$PSScriptRoot/templates/terraform_scripts/eks.tf" -Destination "$kubernetesTerraformPath/eks.tf"
Copy-Item -Path "$PSScriptRoot/templates/terraform_scripts/output.tf" -Destination "$kubernetesTerraformPath/output.tf"
Copy-Item -Path "$PSScriptRoot/templates/terraform_scripts/variables.tf" -Destination "$kubernetesTerraformPath/variables.tf"
# Load terraform state from config folder
Sync-State -Component "eks" -EnvPrefix $(Get-EnvMapValue -Map $config -Key "environment.prefix")
