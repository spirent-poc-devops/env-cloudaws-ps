#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [hashtable] $Config,

    [Parameter(Mandatory = $true, Position = 1)]
    [hashtable] $Resources
)

# Read parameters
$namespace = Get-EnvMapValue -Map $config -Key "k8s.namespace"
$grafana_version = Get-EnvMapValue -Map $config -Key "metrics.grafana_version"

$templateParams = @{ 
    namespace=$namespace;
    grafana_version=$grafana_version 
}

# Generate template
Build-EnvTemplate -InputPath "$PSScriptRoot/templates/metrics.yml" -OutputPath "$PSScriptRoot/../temp/metrics.yml" -Params1 $templateParams
