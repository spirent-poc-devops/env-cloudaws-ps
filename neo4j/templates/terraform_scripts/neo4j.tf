variable "neo4j_instance_ami" {
  type        = string
  description = "Neo4j vm instance ami"
}

variable "neo4j_instance_type" {
  type        = string
  description = "Neo4j vm instance type"
}

variable "neo4j_keypair_name" {
  type        = string
  description = "Neo4j vm instance keypair name"
}

variable "neo4j_associate_public_ip_address" {
  type        = string
  description = "Define is Neo4j vm instance have public ip [true/false]"
}

variable "neo4j_subnet_id" {
  type        = string
  description = "Neo4j vm subnet id"
}

variable "neo4j_volume_size" {
  type        = string
  description = "Neo4j vm instance volume size"
}

variable "env_nameprefix" {
  type        = string
  description = "Environment prefix used for naming resources"
}

variable "aws_vpc" {
  type        = string
  description = "Neo4j vm AWS vpc"
}

variable "neo4j_ebs_snapshot_id" {
  type        = string
  description = "Snapshot id of AMI's EBS"
}

variable "mgmt_subnet_cidr" {
  type        = string
  description = "Management station CIDR"
}

# Configure AWS instance
resource "aws_instance" "neo4j_<%=node_index%>" {
  ami                         = var.neo4j_instance_ami
  instance_type               = var.neo4j_instance_type
  key_name                    = var.neo4j_keypair_name
  associate_public_ip_address = var.neo4j_associate_public_ip_address
  subnet_id                   = var.neo4j_subnet_id
  vpc_security_group_ids      = [aws_security_group.neo4j_<%=node_index%>.id]
  ebs_block_device {
    device_name = "/dev/sda1"
    volume_size = var.neo4j_volume_size
    snapshot_id = var.neo4j_ebs_snapshot_id
  }

  tags = {
    Environment = var.env_nameprefix
    Name        = "${var.env_nameprefix}_neo4j_<%=node_index%>"
  }
}

# Configure AWS security group
resource "aws_security_group" "neo4j_<%=node_index%>" {
  name        = "${var.env_nameprefix}_neo4j_<%=node_index%>"
  description = "neo4j access"
  vpc_id      = var.aws_vpc

  ingress {
    description = "access_from_mgmt_and_other_neo4j_nodes"
    from_port   = 0
    to_port     = 65535
    protocol    = "tcp"
    cidr_blocks = [var.mgmt_subnet_cidr, var.neo4j_subnet_cidr]
  }

  ingress {
    description = "access_to_neo4j"
    from_port   = 7474
    to_port     = 7474
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress { 
    description = "access_to_neo4j_bolt"
    from_port   = 7687
    to_port     = 7687
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }  

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Environment = var.env_nameprefix
    Name        = var.env_nameprefix_neo4j_<%=node_index%>_sg"
  }
}

# Save neo4j instance id
output "neo4j_<%=node_index%>_id" {
  value       = aws_instance.neo4j_<%=node_index%>.id
  description = "The unique identifier of the neo4j virtual machine."
}

# Save neo4j security group id
output "neo4j_<%=node_index%>_sg_id" {
  value       = aws_security_group.neo4j_<%=node_index%>.id
  description = "The unique identifier of the neo4j security group."
}

# Save neo4j instance private ip
output "neo4j_<%=node_index%>_private_ip" {
  value       = aws_instance.neo4j_<%=node_index%>.private_ip
  description = "The private IP address of the neo4j virtual machine."
}

# Save neo4j instance public ip
output "neo4j_<%=node_index%>_public_ip" {
  value       = aws_instance.neo4j_<%=node_index%>.public_ip
  description = "The public IP address of the neo4j virtual machine."
}
