#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [string] $ConfigPath,

    [Parameter(Mandatory = $true, Position = 1)]
    [string] $ResourcePath
)

# Update resources
. "$PSScriptRoot/update_resources.ps1" -ConfigPath $ConfigPath -ResourcePath $ResourcePath

#  Update service
. "$PSScriptRoot/update_service.ps1" -ConfigPath $ConfigPath -ResourcePath $ResourcePath
