#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [string] $ConfigPath,

    [Parameter(Mandatory = $true, Position = 1)]
    [string] $ResourcePath
)

# Read config and resources
$config = Read-EnvConfig -ConfigPath $ConfigPath
$resources = Read-EnvResources -ResourcePath $ResourcePath

# Prepare hosts file if missing
if (!(Test-Path -Path "$path/../temp/neo4j_ansible_hosts")) {
    # Prepare ansible hosts file and whitelist nodes
    . "$PSScriptRoot/ansible.ps1" -Config $config -Resources $resources
}

Copy-Item -Path "$($path)/templates/neo4j_uninstall_playbook.yml" -Destination "$($path)/../temp/neo4j_uninstall_playbook.yml"

# Destroy neo4j database
if ($config["extended_ansible_logs"]) {
    ansible-playbook -v -i "$path/../temp/neo4j_ansible_hosts" "$path/../temp/neo4j_uninstall_playbook.yml"
} else {
    ansible-playbook -i "$path/../temp/neo4j_ansible_hosts" "$path/../temp/neo4j_uninstall_playbook.yml"
}

# Delete results and save resource file to disk
if (Test-EnvMapValue -Map $resources -Key "neo4j") {
    Remove-EnvMapValue -Map $resources -Key "neo4j.endpoint"
    Remove-EnvMapValue -Map $resources -Key "neo4j.inventory"
    Remove-EnvMapValue -Map $resources -Key "neo4j.host"
    Remove-EnvMapValue -Map $resources -Key "neo4j.port"
    Remove-EnvMapValue -Map $resources -Key "neo4j.bolt_port"
}
Write-EnvResources -ResourcePath $ResourcePath -Resources $resources
