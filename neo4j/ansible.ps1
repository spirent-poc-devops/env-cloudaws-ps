#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [hashtable] $Config,

    [Parameter(Mandatory = $true, Position = 2)]
    [hashtable] $Resources
)

# Prepare hosts file
$ansible_inventory = @("[neo4j]")
$neo4jVMCount = $(Get-EnvMapValue -Map $config -Key "neo4j.nodes_count")
for ($i = 0; $i -lt $neo4jVMCount; $i++) {
    $ansible_inventory += "$i ansible_host=$(Get-EnvMapValue -Map $resources -Key "neo4j.node_$i`.private_ip") ansible_ssh_user=$(Get-EnvMapValue -Map $config -Key "neo4j.username") ansible_ssh_private_key_file=$(Get-EnvMapValue -Map $resources -Key "neo4j.keypair_name")"
}
Set-Content -Path "$path/../temp/neo4j_ansible_hosts" -Value $ansible_inventory

# Whitelist nodes
if ($config["extended_ansible_logs"]) {
    ansible-playbook -v -i "$path/../temp/neo4j_ansible_hosts" "$path/templates/ssh_keyscan_playbook.yml"
} else {
    ansible-playbook -i "$path/../temp/neo4j_ansible_hosts" "$path/templates/ssh_keyscan_playbook.yml"
}
