#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [string] $ConfigPath,

    [Parameter(Mandatory = $true, Position = 1)]
    [string] $ResourcePath
)

# Read config and resources
$config = Read-EnvConfig -ConfigPath $ConfigPath
$resources = Read-EnvResources -ResourcePath $ResourcePath

################################## neo4j ##################################
# Prepare terraform files and set terraform env vars
. "$PSScriptRoot/terraform.ps1" -Config $config -Resources $resources

# Delete aws resources
Set-Location $neo4jTerraformPath

terraform init
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "neo4j" "Can't initialize terraform. Watch logs above or check '$neo4jTerraformPath' folder content."
}

terraform plan
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "neo4j" "Can't execute terraform plan. Watch logs above or check '$neo4jTerraformPath' folder content."
}

terraform destroy -auto-approve
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "neo4j" "Can't delete cloud resources. Watch logs above or check '$neo4jTerraformPath' folder content."
}

Set-Location "$PSScriptRoot/.."

# Delete terraform state from config folder
Remove-State -Component "neo4j" -EnvPrefix $(Get-EnvMapValue -Map $config -Key "environment.prefix")

for ($i = 0; $i -lt $neo4jVMCount; $i++) {
    # Remove from resources
    if (Test-EnvMapValue -Map $resources -Key "neo4j") {
        if (Test-EnvMapValue -Map $resources -Key "neo4j.node_$($i)") {
            Remove-EnvMapValue -Map $resources -Key "neo4j.node_$($i).private_ip"
            Remove-EnvMapValue -Map $resources -Key "neo4j.node_$($i).public_ip"
            Remove-EnvMapValue -Map $resources -Key "neo4j.node_$($i).id"
            Remove-EnvMapValue -Map $resources -Key "neo4j.node_$($i).sg_id"
        }
    }
}
        
Write-EnvResources -ResourcePath $ResourcePath -Resources $resources
###################################################################

################################## Keypair ##################################
# Prepare terraform files and set terraform env vars
. "$PSScriptRoot/terraform.ps1" -Config $config -Resources $resources

# Delete aws resources
Set-Location $keypairTerraformPath

terraform init
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "neo4j" "Can't initialize terraform. Watch logs above or check '$keypairTerraformPath' folder content."
}

terraform plan
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "neo4j" "Can't execute terraform plan. Watch logs above or check '$keypairTerraformPath' folder content."
}

terraform destroy -auto-approve
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "neo4j" "Can't delete cloud resources. Watch logs above or check '$keypairTerraformPath' folder content."
}

Set-Location "$PSScriptRoot/.."

# Delete terraform state from config folder
Remove-State -Component "keypair_neo4j" -EnvPrefix $(Get-EnvMapValue -Map $config -Key "environment.prefix")

# Delete results and save resource file to disk

if (Test-EnvMapValue -Map $resources -Key "neo4j") {
    Remove-EnvMapValue -Map $resources -Key "neo4j.keypair_name"
    Write-EnvResources -ResourcePath $ResourcePath -Resources $resources
}
###################################################################