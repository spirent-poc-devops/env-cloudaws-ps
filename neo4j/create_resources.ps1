#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [string] $ConfigPath,

    [Parameter(Mandatory = $true, Position = 1)]
    [string] $ResourcePath
)

# Read config and resources
$config = Read-EnvConfig -ConfigPath $ConfigPath
$resources = Read-EnvResources -ResourcePath $ResourcePath

############################## Keypair #####################################
# Prepare terraform files and set terraform env vars
. "$PSScriptRoot/terraform.ps1" -Config $config -Resources $resources

Set-Location $keypairTerraformPath

# Initialize terraform
Write-Host "Initializing terraform..."
terraform init
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "neo4j" "Can't initialize terraform. Watch logs above or check '$keypairTerraformPath' folder content."
}

# Plan cloud resources creation
Write-Host "Executing terraform plan command..."
terraform plan
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "neo4j" "Can't execute terraform plan. Watch logs above or check '$keypairTerraformPath' folder content."

}

# Create cloud resources
Write-Host "Creating cloud resources by terraform..."
terraform apply -auto-approve
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
        Write-EnvError -Component "neo4j" "Can't create cloud resources. Watch logs above or check '$keypairTerraformPath' folder content."
}

# Get keypair name
$keypair=$(terraform output keypair_name).Trim('"')
Set-EnvMapValue -Map $resources -Key "neo4j.keypair_name" -Value $keypair

Set-Location "$($path)/.."

Write-EnvResources -ResourcePath $ResourcePath -Resources $resources
###################################################################


############################### Neo4j ####################################
# Prepare terraform files and set terraform env vars
. "$PSScriptRoot/terraform.ps1" -Config $config -Resources $resources

Set-Location $neo4jTerraformPath

# Initialize terraform
Write-Host "Initializing terraform..."
terraform init
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "neo4j" "Can't initialize terraform. Watch logs above or check '$keypairTerraformPath' folder content."
}

# Plan cloud resources creation
Write-Host "Executing terraform plan command..."
terraform plan
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "neo4j" "Can't execute terraform plan. Watch logs above or check '$keypairTerraformPath' folder content."
}

# Create cloud resources
Write-Host "Creating cloud resources by terraform..."
terraform apply -auto-approve
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "neo4j" "Can't create cloud resources. Watch logs above or check '$keypairTerraformPath' folder content."
}

for ($i = 0; $i -lt $neo4jVMCount; $i++) {
    # Get outputs
    $neo4j_id=$(terraform output "neo4j_$($i)_id").Trim('"')
    $neo4j_private_ip=$(terraform output "neo4j_$($i)_private_ip").Trim('"')
    $neo4j_public_ip=$(terraform output "neo4j_$($i)_public_ip").Trim('"')
    $neo4j_sg_id=$(terraform output "neo4j_$($i)_sg_id").Trim('"')

    Set-EnvMapValue -Map $resources -Key "neo4j.node_$($i).id" -Value $neo4j_id
    Set-EnvMapValue -Map $resources -Key "neo4j.node_$($i).private_ip" -Value $neo4j_private_ip
    Set-EnvMapValue -Map $resources -Key "neo4j.node_$($i).public_ip" -Value $neo4j_public_ip
    Set-EnvMapValue -Map $resources -Key "neo4j.node_$($i).sg_id" -Value $neo4j_sg_id
}

Set-Location "$PSScriptRoot/.."

# Save terraform state to config folder
Save-State -Component "neo4j" -EnvPrefix $(Get-EnvMapValue -Map $config -Key "environment.prefix")

Write-EnvResources -ResourcePath $ResourcePath -Resources $resources
