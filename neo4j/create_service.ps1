#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [string] $ConfigPath,

    [Parameter(Mandatory = $true, Position = 1)]
    [string] $ResourcePath
)

# Read config and resources
$config = Read-EnvConfig -ConfigPath $ConfigPath
$resources = Read-EnvResources -ResourcePath $ResourcePath

# Prepare ansible hosts file and whitelist nodes
. "$PSScriptRoot/ansible.ps1" -Config $config -Resources $resources

# Install neo4j database
$neo4jVMCount = $(Get-EnvMapValue -Map $config -Key "neo4j.nodes_count")
$hw_neo4j_discovery_members = ""
for ($i = 0; $i -lt $neo4jVMCount; $i++) {
    $hw_neo4j_discovery_members += "$(Get-EnvMapValue -Map $resources -Key "neo4j.node_$i`.private_ip"):5000,"
}
$hw_neo4j_discovery_members = $hw_neo4j_discovery_members.Substring(0, $hw_neo4j_discovery_members.Length - 1)

$templateParams = @{ 
    hw_neo4j_discovery_members=$hw_neo4j_discovery_members
}

Build-EnvTemplate -InputPath "$($path)/templates/neo4j_install_playbook.yml" -OutputPath "$($path)/../temp/neo4j_install_playbook.yml" -Params1 $templateParams
if ($config["extended_ansible_logs"]) {
    ansible-playbook -v -i "$path/../temp/neo4j_ansible_hosts" "$path/../temp/neo4j_install_playbook.yml"
} else {
    ansible-playbook -i "$path/../temp/neo4j_ansible_hosts" "$path/../temp/neo4j_install_playbook.yml"
}

# Write neo4j resources
$port="7474"
$bolt_port="7687"
$ip=(Get-EnvMapValue -Map $config -Key "neo4j.node_0.private_ip")
$endpoint = "$($ip):$($port)"

Set-EnvMapValue -Map $resources -Key "neo4j.host" -Value $ip
Set-EnvMapValue -Map $resources -Key "neo4j.port" -Value $port
Set-EnvMapValue -Map $resources -Key "neo4j.bolt_port" -Value $bolt_port
Set-EnvMapValue -Map $resources -Key "neo4j.endpoint" -Value $endpoint
Set-EnvMapValue -Map $resources -Key "neo4j.inventory" -Value $ansible_inventory

Write-EnvResources -ResourcePath $ResourcePath -Resources $resources
