#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [hashtable] $Config,

    [Parameter(Mandatory = $true, Position = 2)]
    [hashtable] $Resources
)

############################## Keypair #####################################
$elasticsearchTerraformPath = "$PSScriptRoot/../temp/elasticsearch_$(Get-EnvMapValue -Map $config -Key "environment.prefix")"
if (!(Test-Path $elasticsearchTerraformPath)) {
    $null = New-Item $elasticsearchTerraformPath -ItemType "directory"
} 

# Select cloud terraform templates
$env:TF_VAR_aws_region = Get-EnvMapValue -Map $config -Key "aws.region"
$env:TF_VAR_aws_cred_file = Get-EnvMapValue -Map $config -Key "aws.credentials_file"
$env:TF_VAR_aws_profile = Get-EnvMapValue -Map $config -Key "aws.profile"
$env:TF_VAR_aws_vpc = Get-EnvMapValue -Map $config -Key "aws.vpc"
$env:TF_VAR_env_nameprefix = Get-EnvMapValue -Map $config -Key "environment.prefix"
$env:TF_VAR_mgmt_subnet_cidr = Get-EnvMapValue -Map $config -Key "mgmtstation.subnet_cidr"
$env:TF_VAR_k8s_subnet_cidr = Get-EnvMapValue -Map $config -Key "eks.subnet_cidr"
$env:TF_VAR_name_prefix = Get-EnvMapValue -Map $config -Key "logging.name_prefix"
$env:TF_VAR_subnet_ids = Get-EnvMapValue -Map $config -Key "logging.subnet_ids"
$env:TF_VAR_version = Get-EnvMapValue -Map $config -Key "logging.version"
$env:TF_VAR_instance_type = Get-EnvMapValue -Map $config -Key "logging.instance_type"
$env:TF_VAR_instance_count = Get-EnvMapValue -Map $config -Key "logging.instance_count"
$env:TF_VAR_ebs_volume_size = Get-EnvMapValue -Map $config -Key "logging.ebs_volume_size"

Copy-Item -Path "$($path)/templates/terraform_scripts/provider.tf" -Destination "$($path)/../temp/elasticsearch_tf/provider.tf" -Params1 $templateParams
Copy-Item -Path "$($path)/templates/terraform_scripts/elasticsearch.tf" -Destination "$($path)/../temp/elasticsearch_tf/elasticsearch.tf" -Params1 $templateParams

# Load terraform state from config folder
Sync-State -Component "logging" -EnvPrefix $(Get-EnvMapValue -Map $config -Key "environment.prefix")
