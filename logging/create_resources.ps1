#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [string] $ConfigPath,

    [Parameter(Mandatory = $true, Position = 1)]
    [string] $ResourcePath
)

# Read config and resources
$config = Read-EnvConfig -ConfigPath $ConfigPath
$resources = Read-EnvResources -ResourcePath $ResourcePath

# Prepare terraform files and set terraform env vars
. "$PSScriptRoot/terraform.ps1" -Config $config -Resources $resources

# Initialize terraform
Set-Location $elasticsearchTerraformPath
Write-Host "Initializing terraform..."
terraform init
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "logging" "Can't initialize terraform. Watch logs above or check '$elasticsearchTerraformPath' folder content."
}

# Plan cloud resources creation
Write-Host "Executing terraform plan command..."
terraform plan
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "logging" "Can't execute terraform plan. Watch logs above or check '$elasticsearchTerraformPath' folder content."
}

# Create cloud resources
Write-Host "Creating cloud resources by terraform..."
terraform apply -auto-approve
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "logging" "Can't create cloud resources. Watch logs above or check '$elasticsearchTerraformPath' folder content."
}

# Get outputs
$elasticsearch_id=$(terraform output "domain_id").Trim('"')
$elasticsearch_name=$(terraform output "domain_name").Trim('"')
$elasticsearch_endpoint=$(terraform output "domain_endpoint").Trim('"')

Set-EnvMapValue -Map $resources -Key "logging.id" -Value $elasticsearch_id
Set-EnvMapValue -Map $resources -Key "logging.name" -Value $elasticsearch_name
Set-EnvMapValue -Map $resources -Key "logging.endpoint" -Value $elasticsearch_endpoint

Set-Location "$($path)/.."

# Save terraform state to config folder
Save-State -Component "keypair_kafka" -EnvPrefix $(Get-EnvMapValue -Map $config -Key "environment.prefix")

# Save resources and config files
Write-EnvResources -ResourcePath $ResourcePath -Resources $resources
