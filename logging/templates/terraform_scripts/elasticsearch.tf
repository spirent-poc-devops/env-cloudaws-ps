variable "env_nameprefix" {
  type        = string
  description = "Environment prefix"
}

variable "aws_vpc" {
  type        = string
  description = "AWS vpc"
}

variable "mgmt_subnet_cidr" {
  type        = string
  description = "Management station CIDR"
}

variable "k8s_subnet_cidr" {
  type        = string
  description = "Kubernetes CIDR"
}

variable "name_prefix" {
  type        = string
  description = "Elasticsearch resources prefix"
}

variable "version" {
  type        = string
  description = "Elasticsearch version"
}

variable "instance_type" {
  type        = string
  description = "Elasticsearch instance type"
}

variable "instance_count" {
  type        = string
  description = "Elasticsearch instance count"
}

variable "ebs_volume_size" {
  type        = string
  description = "Elasticsearch volume size"
}

# Configure AWS security group
resource "aws_security_group" "elasticsearch" {
  name        = "${var.env_nameprefix}_elasticsearch"
  description = "elasticsearch access"
  vpc_id      = var.aws_vpc

  ingress {
    description = "access_from_mgmt_and_k8s"
    from_port   = 0
    to_port     = 65535
    protocol    = "tcp"
    cidr_blocks = ["${var.mgmt_subnet_cidr}", "${var.k8s_subnet_cidr}"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Environment = var.env_nameprefix
    Name        = "${var.env_nameprefix}_elasticsearch_sg"
  }
}

module "elasticsearch" {
  source = "cloudposse/elasticsearch/aws"
  namespace               = "var.name_prefix"
  stage                   = "cloud"
  name                    = "es"
  security_groups         = [aws_security_group.elasticsearch.id]
  vpc_id                  = var.aws_vpc
  subnet_ids              = var.subnet_ids
  zone_awareness_enabled  = "true"
  elasticsearch_version   = var.version
  instance_type           = var.instance_type
  instance_count          = var.instance_count
  ebs_volume_size         = var.ebs_volume_size
  iam_actions             = ["es:ESHttpGet", "es:ESHttpPut", "es:ESHttpPost"]
  encrypt_at_rest_enabled = true

  advanced_options = {
    "rest.action.multi.allow_explicit_index" = "true"
  }
}

output "domain_id" {
  value       = module.elasticsearch.domain_id
  description = "Unique identifier for the Elasticsearch domain"
}

output "domain_name" {
  value       = module.elasticsearch.domain_name
  description = "Name of the Elasticsearch domain"
}

output "domain_endpoint" {
  value       = module.elasticsearch.domain_endpoint
  description = "Domain-specific endpoint used to submit index, search, and data upload requests"
}
