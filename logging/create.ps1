#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [string] $ConfigPath,

    [Parameter(Mandatory = $true, Position = 1)]
    [string] $ResourcePath
)

# Create resources
# . "$PSScriptRoot/create_resources.ps1" -ConfigPath $ConfigPath -ResourcePath $ResourcePath

#  Create service
. "$PSScriptRoot/create_service.ps1" -ConfigPath $ConfigPath -ResourcePath $ResourcePath
